//
//  ShareData.swift
//  CastTV
//
//  Created by Kiu on 8/4/21.
//

import Foundation

class Share: NSObject, NSCoding { // 1
    var url: String
    
    init(url: String) {
        self.url = url
    }
    
    enum Key: String {
        case url
    }
    // 2
    required init(coder aDecoder: NSCoder) {
        url = aDecoder.decodeObject(forKey: Key.url.rawValue) as? String ?? ""
    }
    // 3
    func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: Key.url.rawValue)
    }
}
let kBroadcastStarted = "kBroadcastStarted"
let kAppGroup = "group.com.tvt.citygo"
