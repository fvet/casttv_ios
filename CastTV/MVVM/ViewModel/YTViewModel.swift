//
//  YTViewModel.swift
//  CastTV
//
//  Created by Kiu on 7/19/21.
//

import Foundation

class YTViewModel {
    
    var youtubeURL : String = ""
    var keyword : String = ""
    var api = YoutubeAPI()
    
    func getMP4Link(_ complete: @escaping (Result<ParserVideo, NetworkError>)->()) {
        let params : [String : Any] = ["url" : youtubeURL]
        api.getMP4Link(params: params) { (result) in
            complete(result)
        }
    }
    
    func searchVideo(_ complete: @escaping (Result<SearchResponse, NetworkError>)->()) {
        
        let params : [String : Any] = ["q" : keyword,
                                       "maxResults" : 25,
                                       "part" : "snippet",
                                       "key" : kAPIKEY]
        api.searchVideo(params: params) { (result) in
            complete(result)
        }
    }
}
