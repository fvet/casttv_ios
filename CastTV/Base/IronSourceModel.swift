//
//  IronSourceModel.swift
//  CastTV
//
//  Created by Admin on 20/08/2021.
//

import Foundation
//
//  GoogleAdsModel.swift
//  iTopMucsic
//
//  Created by Hau Nguyen on 8/23/16.
//  Copyright © 2016 Hau Nguyen. All rights reserved.
//

import UIKit

var kAPPKEY :String {
    if let IRONSOURCE_ID = RemoteConfigManager.shared.getValue(fromKey: .IRONSOURCE_ID) as String? {
        if IRONSOURCE_ID.count > 0 {
            return IRONSOURCE_ID
        }
    }
    return "1096f5f7d"
}

var IS_ADS_IS_ADMOD :Bool {
    if let ADS_IS_ADMOD = RemoteConfigManager.shared.getValue(fromKey: .ADS_IS_ADMOD) as String? {
        if ADS_IS_ADMOD.count > 0 && ADS_IS_ADMOD == "true"{
            return true
        }
    }
    return false
}

var IS_REMOVE_ADS :Bool {
    if let is_remove_ads = RemoteConfigManager.shared.getValue(fromKey: .is_remove_ads) as String? {
        if is_remove_ads.count > 0 && is_remove_ads == "true"{
            return true
        }
    }
    return false
}
class IronSourceModel: NSObject {
    
    static let sharedInstance = IronSourceModel()
    var viewBannerIS: ISBannerView! = nil

    func setupIronSourceSdk() {
        ISIntegrationHelper.validateIntegration()
        IronSource.setInterstitialDelegate(self)
        IronSource.setBannerDelegate(self)
        IronSource.initWithAppKey(kAPPKEY, adUnits: [IS_BANNER, IS_INTERSTITIAL])
        
        //Load sẵn interstials
        IronSource.loadInterstitial()

    }
    
    func requestIronBannerView(viewController:UIViewController) {
        let BNSize: ISBannerSize = ISBannerSize(description: "BANNER",width:Int(UIScreen.main.bounds.width) ,height:50)
        IronSource.loadBanner(with: viewController, size: BNSize, placement: viewController.nibName)
    }
    
    public func logFunctionName(string: String = #function) {
        print("IronSource Swift Demo App: "+string)
    }
    
    
}
extension IronSourceModel: ISInterstitialDelegate {
    public func interstitialDidLoad() {
        logFunctionName()
        
    }
    
    public func interstitialDidFailToLoadWithError(_ error: Error!) {
        logFunctionName(string: #function+String(describing: error.self))
        
    }
    
    public func interstitialDidOpen() {
        logFunctionName()
        
    }
    
    public func interstitialDidClose() {
        logFunctionName()
        //Sau khi nhaans close phari load lai
        IronSource.loadInterstitial()
    }
    
    public func interstitialDidShow() {
        logFunctionName()
    }
    
    public func interstitialDidFailToShowWithError(_ error: Error!) {
        logFunctionName(string: String(describing: error.self))
    }
    
    public func didClickInterstitial() {
        logFunctionName()
        //Sau khi nhaans close phari load lai
        IronSource.loadInterstitial()

    }
}

extension IronSourceModel: ISBannerDelegate {
    func bannerDidLoad(_ bannerView: ISBannerView!) {
        self.viewBannerIS = bannerView
        // sau khi nó load dc banner thì add vào view
        logFunctionName()
        let heigh = 50
        bannerView.frame = CGRect(x: 0, y: Int(SCREEN_HEIGHT) - SIZEHEIGHT_TABBAR - heigh, width: Int(UIScreen.main.bounds.width), height: heigh)
        SHARE_APPLICATION_DELEGATE.window?.rootViewController?.view.addSubview(bannerView)
    }
    
    public func bannerDidFailToLoadWithError(_ error: Error!) {
        logFunctionName(string: #function+String(describing: Error.self))
    }
    
    public func didClickBanner() {
        logFunctionName()
    }
    
    public func bannerWillPresentScreen() {
        logFunctionName()
    }
    
    public func bannerDidDismissScreen() {
        logFunctionName()
    }
    
    public func bannerWillLeaveApplication() {
        logFunctionName()
    }
    
    public func bannerDidShow() {
        logFunctionName()
    }
    
}
