//
//  BaseTabbarViewController.swift
//  DemoApp
//
//  Created by Kiu on 7/10/21.
//

import UIKit
let STORYBOARD_MAI = UIStoryboard(name: "Main", bundle: nil)

class BaseTabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let remoteVC = STORYBOARD_MAI.instantiateViewController(withIdentifier: "FirstViewController")
//        let navRemote = BaseNavigationVC.init(rootViewController: remoteVC)
//        navRemote.tabBarItem = UITabBarItem.init(title: "Remote", image: #imageLiteral(resourceName: "tab_remote"), tag: 0)

        SHARE_APPLICATION_DELEGATE.remoteVC = RemoteTVVC()
        let navRemote = BaseNavigationVC.init(rootViewController: SHARE_APPLICATION_DELEGATE.remoteVC!)
        navRemote.tabBarItem = UITabBarItem.init(title: "Remote", image: #imageLiteral(resourceName: "tab_remote"), tag: 0)
        
        
        let channelVC = ChanelVC()
        let navChannel = BaseNavigationVC.init(rootViewController: channelVC)
        navChannel.tabBarItem = UITabBarItem.init(title: "Channels", image: #imageLiteral(resourceName: "tab_Channels"), tag: 1)
        
        
        let castVC = CastVC()
        let navCast = BaseNavigationVC.init(rootViewController: castVC)
        navCast.tabBarItem = UITabBarItem.init(title: "Cast", image: #imageLiteral(resourceName: "tab_cast"), tag: 2)

        let settingVC = SettingVC()
        let navSetting = BaseNavigationVC.init(rootViewController: settingVC)
        navSetting.tabBarItem = UITabBarItem.init(title: "Setting", image: #imageLiteral(resourceName: "tab_settings"), tag: 3)
        tabBar.barTintColor = .black
        tabBar.unselectedItemTintColor = .white
        tabBar.tintColor = TINT_COLOR
        self.viewControllers = [navRemote, navChannel, navCast, navSetting]
    }


}
