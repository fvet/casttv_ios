//
//  PremiumVC.swift
//  CastTV
//
//  Created by Kiu on 7/28/21.
//

import UIKit

class TryPremiumVC: BaseViewController {
    
    //MARK: IBOUTLETS
    
    //MARK: OTHER VARIABLES
    

    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
            setupVar()
            setupUI()
            callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
        
    }
    
    func setupUI() {
    
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS

    @IBAction func didTouchClose(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callBackAction?(.close)
        }
    }
    
    @IBAction func didTouchTry(_ sender: Any) {
        self.dismiss(animated: true) {
            self.callBackAction?(nil)
        }
    }
}
