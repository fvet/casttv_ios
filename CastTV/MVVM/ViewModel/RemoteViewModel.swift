//
//  RemoteViewModel.swift
//  CastTV
//
//  Created by Admin on 26/07/2021.
//

import Foundation
class RemoteViewModel {
    
    var ip : String {
        return Token().currentTV?.address_ip ?? ""
    }
    var mac : String = ""
    var token : String = "1234"
    var api = RemoteAPI()
    
    func getKeyboardText(_ complete: @escaping (Result<TVModelResponse, NetworkError>)->()) {
        
        let params : [String : Any] = [
            "method": "getTextForm",
            "id": 60,
            "params": [[:]],
            "version": "1.1"
        ]

        api.getPowerStatus(url: "http://\(ip)/sony/appControl", params) { result in
            complete(result)
        }
    }
    
    func sendTextKeyBoard(key: String , _ complete: @escaping (Result<ChanelListModel, NetworkError>)->()) {
        
        let params : [String : Any] = [
            "method": "setTextForm",
            "id": 601,
            "params": [key],
            "version": "1.0"
        ]
        api.getApplicationList(url: "http://\(ip)/sony/appControl", params) { result in
            complete(result)
        }
    }
    
    func openApp(_ url: String, _ complete: @escaping (Result<ChanelListModel, NetworkError>)->()) {
        
        let params : [String : Any] = [
            "method": "setActiveApp",
            "id": 601,
            "params": [["uri": url]],
            "version": "1.0"
        ]
        api.getApplicationList(url: "http://\(ip)/sony/appControl", params) { result in
            complete(result)
        }
    }
    
    func getApplicationList(_ complete: @escaping (Result<ChanelListModel, NetworkError>)->()) {
        
        let params : [String : Any] = [
            "method": "getApplicationList",
            "id": 70,
            "params": [],
            "version": "1.0"
        ]
        api.getApplicationList(url: "http://\(ip)/sony/appControl", params) { result in
            complete(result)
        }
    }
    
    
    func getPowerStatus(_ complete: @escaping (Result<TVModelResponse, NetworkError>)->()) {
        
        let params : [String : Any] = [
            "method": "getPowerStatus",
            "id": 50,
            "params": [],
            "version": "1.0"
        ]
        api.getPowerStatus(url: "http://\(ip)/sony/system", params) { result in
            complete(result)
        }
    }
    
    func setPowerStatus(onTV: Bool, _ complete: @escaping (Result<TVModelResponse, NetworkError>)->()) {
        
        let params : [String : Any] = [
            "method": "setPowerStatus",
            "id": 55,
            "params": [["status": onTV]],
            "version": "1.0"
        ]
        api.getPowerStatus(url: "http://\(ip)/sony/system", params) { result in
            complete(result)
        }
    }
    
    func getToken(_ complete: @escaping (Result<RemoteModel, NetworkError>)->()) {
        
        let params : [String : Any] = ["ip" : ip,
                                       "mac" : mac,
                                       "token" : token
        ]
        api.getToken(params) { result in
            complete(result)
        }
    }
    
    func sendComandKey(_ key: String, _ complete: @escaping (Result<RemoteModel, NetworkError>)->()) {
        
        let params : [String : Any] = ["ip" : ip,
                                       "mac" : mac,
                                       "token" : token,
                                       "key":key]
        api.getToken(params) { result in
            complete(result)
        }
    }
    
}
