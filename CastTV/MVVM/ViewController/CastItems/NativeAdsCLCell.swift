//
//  NativeAdsCLCell.swift
//  CastTV
//
//  Created by Admin on 30/07/2021.
//

import UIKit
import GoogleMobileAds

class NativeAdsCLCell: UICollectionViewCell {
    var adLoader: GADAdLoader!
    var parentView: UIViewController?
    @IBOutlet weak var nativeAdView: GADNativeAdView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    
    }
    
    func fillData() {
        guard let vc = self.parentView else {
            return
        }

        adLoader = GADAdLoader(
          adUnitID: IDNativeAds, rootViewController: vc,
          adTypes: [.native], options: nil)
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }

}

extension NativeAdsCLCell: GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print(error)
    }
    
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {

//        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline

        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent

        // showing or hiding them.
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil

        nativeAdView.nativeAd = nativeAd


      }
}

