//
//  NotConnectVC.swift
//  CastTV
//
//  Created by Kiu on 7/25/21.
//

import UIKit
import HaishinKit

class NotConnectVC: BaseViewController {
    
    //MARK: IBOUTLETS
    
    //MARK: OTHER VARIABLES
    

    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
            setupVar()
            setupUI()
            callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
    }
    
    func setupUI() {
    
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    @IBAction func didTouchConnect(_ sender: Any) {
        callBackAction?(nil)
    }
}
