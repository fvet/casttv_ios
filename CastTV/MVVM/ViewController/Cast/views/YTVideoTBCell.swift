//
//  YTVideoTBCell.swift
//  CastTV
//
//  Created by Kiu on 7/19/21.
//

import UIKit

class YTVideoTBCell: UITableViewCell {

    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbChannel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(_ model : YTItem) {
        imgVideo?.kfImageURL(model.snippet?.thumbnails?.medium?.url)
        lbTitle.text = model.snippet?.title
        lbChannel.text = model.snippet?.channelTitle
    }
}
