//
//  RemoteTVVC.swift
//  CastTV
//
//  Created by Admin on 19/07/2021.
//

import UIKit
import SystemConfiguration.CaptiveNetwork
import Toast_Swift
import AudioToolbox
import GoogleMobileAds

enum TVTYPE {
    case SONY
    case SAMSUNG
}

enum KEYS {
    case ASSISTANT // MIC
    case ON_OF
    case ACTION123
    case INPUT
    case KEYBOARD
    case BACK
    case HOME
    case SETTING
    case VOL_UP
    case VOL_DOWN
    case MUTE
    case REWIND
    case FORWARD
    case PLAY
    case CH_UP
    case CH_DOWN
}

enum KEYS_SONY:String {
    case ASSISTANT  = "AAAAAgAAAMQAAAA7Aw=="
    case ON_OF = "AAAAAQAAAAEAAAAVAw=="
    case ACTION123 = "ACTION123"
    case INPUT = "AAAAAQAAAAEAAAAlAw=="
    case KEYBOARD = "KEYBOARD"
    case BACK = "AAAAAgAAAJcAAAAjAw=="
    case HOME = "AAAAAQAAAAEAAABgAw=="
    case SETTING = "AAAAAgAAAMQAAABLAw=="
    case VOL_UP = "AAAAAQAAAAEAAAASAw=="
    case VOL_DOWN = "AAAAAQAAAAEAAAATAw=="
    case MUTE = "AAAAAQAAAAEAAAAUAw=="
    case REWIND = "AAAAAgAAAJcAAAAbAw=="
    case FORWARD = "AAAAAgAAAJcAAAAcAw=="
    case PLAY = "AAAAAgAAAJcAAAAaAw=="
    case CH_UP = "AAAAAQAAAAEAAAAQAw=="
    case CH_DOWN = "AAAAAQAAAAEAAAARAw=="
    
    case UP = "AAAAAQAAAAEAAAB0Aw=="
    case DOWN = "AAAAAQAAAAEAAAB1Aw=="
    case LEFT = "AAAAAQAAAAEAAAA0Aw=="
    case RIGHT = "AAAAAQAAAAEAAAAzAw=="
    case OK = "AAAAAQAAAAEAAABlAw=="

}

enum KEYS_SAMSUNG:String {
    case ASSISTANT  = "AAAAAgAAAMQAAAA7Aw=="
    case ON_OF = "AAAAAQAAAAEAAAAVAw=="
    case ACTION123 = "ACTION123"
    case INPUT = "AAAAAQAAAAEAAAAlAw=="
    case KEYBOARD = "KEYBOARD"
    case BACK = "AAAAAgAAAJcAAAAjAw=="
    case HOME = "AAAAAQAAAAEAAABgAw=="
    case SETTING = "AAAAAgAAAMQAAABLAw=="
    case VOL_UP = "AAAAAQAAAAEAAAASAw=="
    case VOL_DOWN = "AAAAAQAAAAEAAAATAw=="
    case MUTE = "AAAAAQAAAAEAAAAUAw=="
    case REWIND = "AAAAAgAAAJcAAAAbAw=="
    case FORWARD = "AAAAAgAAAJcAAAAcAw=="
    case PLAY = "AAAAAgAAAJcAAAAaAw=="
    case CH_UP = "AAAAAQAAAAEAAAAQAw=="
    case CH_DOWN = "AAAAAQAAAAEAAAARAw=="
    
    case UP = "AAAAAQAAAAEAAAB0Aw=="
    case DOWN = "AAAAAQAAAAEAAAB1Aw=="
    case LEFT = "AAAAAQAAAAEAAAA0Aw=="
    case RIGHT = "AAAAAQAAAAEAAAAzAw=="
    case OK = "AAAAAQAAAAEAAABlAw=="
    
}



class RemoteTVVC: BaseCastVC{
    //MARK: IBOUTLETS
    
    @IBOutlet weak var btnPremium: UIButton!
    @IBOutlet weak var btnTouch: UIButton!
    @IBOutlet weak var contraintHeightAds: NSLayoutConstraint!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var viewOk: UIView!
    @IBOutlet weak var touchPad: UIView!
    @IBOutlet weak var viewAddBanner: GADBannerView!
    @IBOutlet weak var viewAddBannerIron: UIView!
    @IBOutlet weak var remoteTV: UIScrollView!
    var pskcode = ""
    @IBOutlet weak var lbNameTV: UILabel!
    var ipaddress = ""
    var name_tv = "" {
        didSet {
            lbNameTV.text = name_tv == "" ? "Kết nối TV" : name_tv.uppercased()
        }
    }
    var isTouchPad = false {
        didSet {
            viewOk.isHidden = isTouchPad
            touchPad.isHidden = !isTouchPad
            btnTouch.setImage(isTouchPad == false ? #imageLiteral(resourceName: "Touchpad") : #imageLiteral(resourceName: "Touchpad_2"), for: .normal)
        }
    }
    

    //MARK: OTHER VARIABLES
    var discoveryManager : DiscoveryManager?
    var connectedDevice : ConnectableDevice?
    var tvTYpe:TVTYPE = .SONY
    var isConnected = false
    var isStanby = false
    var viewModel = RemoteViewModel()

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.isCast = true
        self.showAds()
        self.checkCastStateUI(isConnected: true)

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupVar()
        self.setupUI()
        // Do any additional setup after loading the view.
        tfNumber.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)

        remoteTV.isHidden = false
        isTouchPad = false
        
        
        
        if Token().currentTV == nil {
            name_tv = ""
            discoveryManager = DiscoveryManager.shared()
            discoveryManager?.startDiscovery()
        }else {
            self.connectTV()
            
        }

    }
    
    func connectTV() {
        if let model = Token().currentTV, model.address_ip != nil {
            viewModel.getPowerStatus { result in
                switch result {
                case .success(let model_response):
                    if model_response.result?.first?.status == "active" || model_response.result?.first?.status == "standby" {
                        self.ipaddress = model.address_ip ?? ""
                        self.name_tv = model.name_tv ?? ""
                        self.isConnected = true
                        if model_response.result?.first?.status == "active" {
                            self.isStanby = false
                        }else {
                            self.isStanby = true
                            self.showAlert(title: "\(model.name_tv ?? "")", message: "Đang ở chế độ standby. Vui lòng bật TV")
                        }
//                        

                        return
                    }
                    break
                default:
                    self.startNewTV()
                    break
                }
            }
        }
    }
    
    func startNewTV() {
        name_tv = ""
        self.castButton.sendActions(for: .touchUpInside)
        return
//        name_tv = ""
//        discoveryManager?.devicePicker()?.delegate = self
//        discoveryManager?.devicePicker()?.showActionSheet(UIButton())

    }
    
    func setupVar() {
        if Configuration.inPremiumUser() == true || Configuration.inRemoveAdsUser() == true {
            self.contraintHeightAds.constant = 0
            self.viewAddBanner.isHidden = true
            self.viewAddBannerIron.isHidden = true
            btnPremium.isHidden = true
        }else {
            btnPremium.isHidden = false

            if IS_REMOVE_ADS == true {
                self.contraintHeightAds.constant = 0
                self.viewAddBanner.isHidden = true
                self.viewAddBannerIron.isHidden = true

            }else {
                self.contraintHeightAds.constant = 50
                self.viewAddBanner.isHidden = !IS_ADS_IS_ADMOD
                self.viewAddBannerIron.isHidden = IS_ADS_IS_ADMOD
                if IS_ADS_IS_ADMOD {
                    GoogleAdsModel.sharedInstance.requestGADBannerView(self.viewAddBanner, viewController: self) { success in
                        //
                    }
                }else {
                    IronSourceModel.sharedInstance.requestIronBannerView(viewController: self)
                }
                 if Configuration.inPremiumUser() == false {
                    self.gotoPremium()
                }
            }
            
           
            
        }

    }
    
    func gotoPremium() {
        let vc = TryPremiumVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.callBackAction = { isClose in
            if isClose != nil {
                self.connectTV()
            }else {
                InAppPerchaseManager.shared.openGetPremium(self)
            }
        }
        present(vc, animated: true, completion: nil)

    }
    
    func setupUI() {
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
        doubleTapGesture.numberOfTapsRequired = 2
        touchPad.addGestureRecognizer(doubleTapGesture)
        
        // Long Press
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        touchPad.addGestureRecognizer(longPressGesture)
        
        // Swipe (right and left)
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        swipeRightGesture.direction = UISwipeGestureRecognizer.Direction.right
        swipeLeftGesture.direction = UISwipeGestureRecognizer.Direction.left
        touchPad.addGestureRecognizer(swipeRightGesture)
        touchPad.addGestureRecognizer(swipeLeftGesture)
        
        let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        swipeUpGesture.direction = UISwipeGestureRecognizer.Direction.up
        swipeDownGesture.direction = UISwipeGestureRecognizer.Direction.down
        touchPad.addGestureRecognizer(swipeUpGesture)
        touchPad.addGestureRecognizer(swipeDownGesture)
    }
    
    // Double tap action
    @objc func handleDoubleTap() {
        self.okAction(UIButton())
    }

    // Long press action
    @objc func handleLongPress(gesture: UILongPressGestureRecognizer) {
        self.okAction(UIButton())
    }
    
    
    // Swipe action
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        
        // example task: animate view off screen
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            self.next(UIButton())
            
        } else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            self.pre(UIButton())
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.up {
            self.up(UIButton())
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            self.down(UIButton())
        }
    }
    
    func showInterstitial() {
        if IS_REMOVE_ADS == true {
            return
        }
        if Configuration.inPremiumUser() == true {
            return
        }
        if Configuration.inRemoveAdsUser() == true {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if IS_ADS_IS_ADMOD {
                GoogleAdsModel.sharedInstance.createAndLoadInterstitial(self)
            }else {
                if IronSource.hasInterstitial() {
                    IronSource.showInterstitial(with: self)
                }
            }
        }
       
    }
    
    func showAds() {
        if let number = UserDefaults.standard.object(forKey: "showAds") as? Int {
            print(number)
            
            if number % 3 == 0  {
                self.showInterstitial()
            }
            
            if number > 100 {
                UserDefaults.standard.set(1, forKey: "showAds")
                UserDefaults.standard.synchronize()
            }else {
                UserDefaults.standard.set(number + 1, forKey: "showAds")
                UserDefaults.standard.synchronize()
            }
            
        }else {
            
            UserDefaults.standard.set(1, forKey: "showAds")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    @IBAction func onOffTV(_ sender: Any) {

        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        if self.isStanby {
            self.isStanby = false
            viewModel.setPowerStatus(onTV: true) { result in
                //
            }
        }else {
            self.isStanby = true
            sendCommand(command: "AAAAAQAAAAEAAAAvAw==")
        }
    }
    
    @IBAction func upVolume(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAQAAAAEAAAASAw==")
    }
    
    @IBAction func downVolume(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAQAAAAEAAAATAw==")
    }
    
    @IBAction func home(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAQAAAAEAAABgAw==")
    }
    
    @IBAction func back(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAgAAAJcAAAAjAw==")
       
    }
    
    @IBAction func setting(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAgAAAMQAAABLAw==")
    }
    @IBAction func input(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAQAAAAEAAAAlAw==")
    }
    
    
    @IBAction func up(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAQAAAAEAAAB0Aw==")
    }
    
    @IBAction func down(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAQAAAAEAAAB1Aw==")
    }
    
    @IBAction func next(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAQAAAAEAAAAzAw==")
    }
    
    @IBAction func pre(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.sendCommand(command: "AAAAAQAAAAEAAAA0Aw==")
    }
    
    
    @IBAction func okAction(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAQAAAAEAAABlAw==")
    }
    
    @IBAction func upCH(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAQAAAAEAAAAQAw==")
        
    }
    
    @IBAction func downCH(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAQAAAAEAAAARAw==")
    }
    
    @IBAction func play(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAgAAAJcAAAAaAw==")
    }
    
    @IBAction func forward(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAgAAAJcAAAAcAw==")
    }
    
    @IBAction func rewind(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAgAAAJcAAAAbAw==")
    }
    
    @IBAction func mute(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        sendCommand(command: "AAAAAQAAAAEAAAAUAw==")
    }
    
    @IBAction func Action123(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        if self.isConnected {
            self.tfNumber.becomeFirstResponder()
        }else {
            self.startNewTV()
        }
    }
    
    @IBAction func keyboard(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        let vc = KeyboardVC()
        self.navigationController?.pushViewController(vc)
    }
    
    @IBAction func gotoPremium(_ sender: Any) {
        InAppPerchaseManager.shared.openGetPremium(self)
        
    }
    
    @IBAction func touchPad(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        if self.isConnected {
            if Configuration.inPremiumUser() == true {
                self.isTouchPad = !self.isTouchPad
                return
            }
            InAppPerchaseManager.shared.openGetPremium(self)
        }else {
            self.startNewTV()
        }
    }
    
    @IBAction func green(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        sendCommand(command: "AAAAAgAAAJcAAAAmAw==")
    }
    @IBAction func red(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        sendCommand(command: "AAAAAgAAAJcAAAAlAw==")
    }
    @IBAction func yellow(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        sendCommand(command: "AAAAAgAAAJcAAAAnAw==")
    }
    @IBAction func blue(_ sender: Any) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        sendCommand(command: "AAAAAgAAAJcAAAAkAw==")
    }
    
    
    
    func sendCommand(command:String) {
        self.view.endEditing(true)
        if self.isConnected {
            if self.tvTYpe == .SONY {
                self.sendCommandSony(command: command)
                usleep(80000) //will sleep for .08 seconds

            }else {
    //            RemoteViewModel().sendComandKey(command) { data, error in
    //
    //            }
            }
        }else {
            self.startNewTV()
        }

    }
    
    
    
    func sendCommandSony(command:String) {
//        guard pskcode != "" else {
//            showAlert(title: "Thông báo", message: "Vui lòng nhập mật khẩu TV", buttonTitles: ["OK"], highlightedButtonIndex: nil) { value in
//                self.tfPass.becomeFirstResponder()
//            }
//            return
//        }
        let url = URL(string: "http://\(ipaddress)/sony/IRCC")!
        print("url is set to \(url)")
        var request = URLRequest(url: url)
        request.setValue("text/xml; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("1234", forHTTPHeaderField: "X-Auth-PSK")
        request.setValue("\"urn:schemas-sony-com:service:IRCC:1#X_SendIRCC\"", forHTTPHeaderField: "SOAPACTION")
        request.setValue("TVSideview/2.0.1 CFNetwork/672.0.8Darwin/14.0.0", forHTTPHeaderField: "User-Agent")
        request.httpMethod = "POST"
        
        var xml = "<?xml version=\"1.0\"?>"
        xml += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
        xml += "<s:Body>"
        xml += "<u:X_SendIRCC xmlns:u=\"urn:schemas-sony-com:service:IRCC:1\">"
        xml += "<IRCCCode>\(command)</IRCCCode>"
        xml += "</u:X_SendIRCC>"
        xml += "</s:Body>"
        xml += "</s:Envelope>"
        request.httpBody = xml.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print("error= \(String(describing: error))")
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                var token = Token()
                token.currentTV = nil
                self.isConnected = false
                self.view.makeToast("Mất kết nối TV. Vui lòng kết nối lại")

                
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    if self.isConnected == false {
//                        self.isConnected = true
//                    self.btnConnect.setTitle("Kết nối thành công TV", for: .normal)
//                    }
//                }
                
            }else {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    if self.isConnected == false {
//                        self.showAlert(title: "Kết nối thất bại", message: "Xin kết nối lại TV hoặc nhập lại mật khẩu", buttonTitles: ["OK"], highlightedButtonIndex: nil) { value in
//                            self.tfPass.becomeFirstResponder()
//                        }
//                    }
//                }
            }
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
            

        }
        task.resume()
    }
}
extension RemoteTVVC: DiscoveryManagerDelegate {
    func discoveryManager(_ manager: DiscoveryManager!, didFind device: ConnectableDevice!) {
        
    }
    func discoveryManager(_ manager: DiscoveryManager!, didUpdate device: ConnectableDevice!) {
        
    }
    
    func discoveryManager(_ manager: DiscoveryManager!, didFailWithError error: Error!) {
        
    }
}

extension RemoteTVVC : DevicePickerDelegate {
    func devicePicker(_ picker: DevicePicker!, didSelect device: ConnectableDevice!) {
        connectedDevice = device
        connectedDevice?.delegate = self

        connectedDevice?.connect()
        discoveryManager?.stopDiscovery()
        
        
        
    }
}

extension RemoteTVVC : ConnectableDeviceDelegate {
    func connectableDeviceReady(_ device: ConnectableDevice!) {
        if let address = device.lastKnownIPAddress {
            self.ipaddress = address
            self.name_tv = "\(device.modelName ?? "")"
            self.isConnected = true
            self.view.makeToast("Kết nối TV thành công")
            self.showAds()

            let json = [
                "address_ip" : address,
                "pass_tv" : "123456",
                "name_tv" : device.modelName

            ]
            if let model = json.toCodableObject() as TVModel? {
                var token = Token()
                token.currentTV = model
            }
            
//            connectedDevice?.setPairingType(DeviceServicePairingTypeMixed)
//            var string:[String]  = []
//            if let power = connectedDevice?.powerControl() {
//                print("powerControl")
//                string.append("powerControl")
//            }
//            if let power = connectedDevice?.volumeControl(){
//                print("volumeControl")
//                string.append("volumeControl")
//
//            }
//            if let power = connectedDevice?.keyControl() {
//                print("keyControl")
//                string.append("keyControl")
//
//            }
//            if let power = connectedDevice?.launcher() {
//                power.getAppList { app in
//                    print(app)
//                } failure: { error in
//                    print(error)
//                }
//
//                print("launcher")
//                string.append("launcher")
//
//            }
//            if let power = connectedDevice?.externalInputControl() {
//                print("externalInputControl")
//                string.append("externalInputControl")
//            }
//
//            if let power = connectedDevice?.mediaControl() {
//                print("mediaControl")
//                string.append("mediaControl")
//            }
//
//            if let power = connectedDevice?.mediaPlayer() {
//                print("mediaPlayer")
//                string.append("mediaPlayer")
//            }
//
//            if let power = connectedDevice?.mouseControl() {
//                print("mouseControl")
//                string.append("mouseControl")
//
//            }
//            if let power = connectedDevice?.webAppLauncher() {
//                print("webAppLauncher")
//                string.append("webAppLauncher")
//            }
//
//            if let power = connectedDevice?.toastControl() {
//                print("toastControl")
//                string.append("toastControl")
//            }
//            if let power = connectedDevice?.textInputControl() {
//                print("textInputControl")
//                string.append("textInputControl")
//
//            }
//            self.showAlert(title: "", message: string.joined(separator: ","))
        }
    }

    func connectableDeviceDisconnected(_ device: ConnectableDevice!, withError error: Error!) {
        print(error)
        print(#function)
    }

    func connectableDevice(_ device: ConnectableDevice!, service: DeviceService!, pairingFailedWithError error: Error!) {
        print(error)
        print(#function)
    }
    
    func connectableDevice(_ device: ConnectableDevice!, service: DeviceService!, didFailConnectWithError error: Error!) {
        print(error)
        print(#function)
    }
    
    func connectableDevice(_ device: ConnectableDevice!, connectionFailedWithError error: Error!) {
        print(error)
        print(#function)
    }

}

extension RemoteTVVC: UITextFieldDelegate {
    @objc func textFieldDidChange() {
        if tfNumber.text?.count == 1 {
            self.command(text: tfNumber.text ?? "")
            self.tfNumber.resignFirstResponder()
            self.view.endEditing(true)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == 0 {
            return true
        }
        return false
    }
    func command(text: String) {
        if let value = Int(text) {
            print(value)
            switch value {
            case 0:
                sendCommand(command: zero)
                break
            case 1:
                sendCommand(command: one)
                break
            case 2:
                sendCommand(command: two)
                break
            case 3:
                sendCommand(command: three)
                break
            case 4:
                sendCommand(command: four)
                break
            case 5:
                sendCommand(command: five)
                break
            case 6:
                sendCommand(command: six)
            case 7:
                sendCommand(command: seven)
            case 8:
                sendCommand(command: eight)
            case 9:
                sendCommand(command: nine)
                break
            default:
                break
            }
        }
    }
    
}
let one = "AAAAAQAAAAEAAAAAAw=="
let two = "AAAAAQAAAAEAAAABAw=="
let three = "AAAAAQAAAAEAAAACAw=="
let four = "AAAAAQAAAAEAAAADAw=="
let five = "AAAAAQAAAAEAAAAEAw=="
let six = "AAAAAQAAAAEAAAAFAw=="
let seven = "AAAAAQAAAAEAAAAGAw=="
let eight = "AAAAAQAAAAEAAAAHAw=="
let nine = "AAAAAQAAAAEAAAAIAw=="
let zero = "AAAAAQAAAAEAAAAJAw=="

