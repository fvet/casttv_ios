//
//  UIImageView+Ext.swift
//  CastTV
//
//  Created by Kiu on 7/19/21.
//

import Foundation
import UIKit
import Kingfisher
extension UIImageView {
    
    func kfImageURL(_ url : String?, placeHolder : UIImage? = nil) {
        guard let imageURLString =  url else {
            return
        }
        if let url = URL.init(string: imageURLString) {
            let processor = DownsamplingImageProcessor(size: self.bounds.size)
            self.kf.indicatorType = .activity
            self.kf.setImage(
                with: url,
                placeholder: placeHolder,
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
            {
                result in
                switch result {
                case .success(let value):
                    self.image = value.image
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        }
    }
}
