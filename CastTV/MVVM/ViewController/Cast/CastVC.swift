//
//  CastVC.swift
//  DemoApp
//
//  Created by Kiu on 7/10/21.
//

import UIKit

import Foundation
import MobileCoreServices
import MediaPlayer
import GoogleMobileAds
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH, SCREEN_HEIGHT)
let SIZEHEIGHT_NAVI = CGFloat(64)


let IS_IPAD: Bool = (UIDevice.current.userInterfaceIdiom == .pad)
let IS_UNSPECIFIED: Bool = (UIDevice.current.userInterfaceIdiom == .unspecified)


let IS_IPHONE: Bool = (UIDevice.current.userInterfaceIdiom == .phone)
let IS_RETINA: Bool = (UIScreen.main.scale >= 2.0)

let IS_IPHONE_4S: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH < 568.0))
let IS_IPHONE_5: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 568.0))
let IS_IPHONE_6: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 667.0))
let IS_IPHONE_6P: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH == 736.0))
let IS_IPHONE_X: Bool = (IS_IPHONE&&(SCREEN_MAX_LENGTH >= 812.0))


let QUANLITY_IMAGE:CGFloat = 0.5
let SIZEHEIGHT_TABBAR = IS_IPHONE_X ? 88 : 49
import ReplayKit

enum CastType : String {
    
    case photo = "Photo"
    case video = "Video"
    case youtube = "Youtube"
    case iptv = "IPTV"
    case sound = "Sound"
    case native_ads = "Native_ads"
    case mirroring = "Screen Mirroring"
    
    
    func getIcon() -> UIImage {
        switch self {
        case .photo:
            return #imageLiteral(resourceName: "cast_photo")
        case .video:
            return #imageLiteral(resourceName: "cast_video")
        case .youtube:
            return #imageLiteral(resourceName: "cast_yt")
        case .iptv:
            return #imageLiteral(resourceName: "cast_ip")
        case .sound:
            return #imageLiteral(resourceName: "cast_sound")
        case .native_ads:
            return #imageLiteral(resourceName: "cast_sound")
            
        case .mirroring:
            return #imageLiteral(resourceName: "cast_Mirroring")
        }
    }
    
    func requiredPremium() -> Bool {
        //        let items : [CastType] = [.youtube, .mirroring]
        let items : [CastType] = [.youtube]
        
        return items.contains(self)
    }
}

class CastVC: BaseCastVC {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var contraintHeightAds: NSLayoutConstraint!
    @IBOutlet weak var viewAddBanner: GADBannerView!
    @IBOutlet weak var viewAddBannerIron: UIView!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(nibWithCellClass: CastItemCLCell.self)
            collectionView.register(nibWithCellClass: NativeAdsCLCell.self)
        }
    }
    
    
    
    //MARK: OTHER VARIABLES
    
    let space : CGFloat = 16
    //    var dataSource : [CastType] = [.photo, .video, .sound, .native_ads, .youtube, .mirroring]
    var dataSource : [CastType] = [.photo, .video, .sound, .native_ads, .youtube, .mirroring]
    
    var mediaPickerController : MPMediaPickerController?
    var broadcastController: RPBroadcastController?
    let userDefaults = UserDefaults.init(suiteName: kAppGroup)
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkCastStateUI(isConnected: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.isCast = true
        setupVar()
        setupUI()
        callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    
    func setupVar() {
        
        if Configuration.inPremiumUser() == true || Configuration.inRemoveAdsUser() == true {
            self.contraintHeightAds.constant = 0
            viewAddBanner.isHidden = true
            viewAddBannerIron.isHidden = true
            dataSource.removeAll(.native_ads)
        }else {
            self.contraintHeightAds.constant = 50
            self.viewAddBanner.isHidden = !IS_ADS_IS_ADMOD
            self.viewAddBannerIron.isHidden = IS_ADS_IS_ADMOD

            if IS_ADS_IS_ADMOD {

                GoogleAdsModel.sharedInstance.requestGADBannerView(self.viewAddBanner, viewController: self) { success in
                    //
                }
            }
        }
    }
    
    func setupUI() {
        navigationItem.title = "Cast"
    }
    
    //MARK - CALL API
    func callAPI() {
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
        
        
    }
    
    
    //MARK: - BUTTON ACTIONS
    
    //    @objc func demo() {
    //        let vc = DemoVC()
    //        self.present(vc, animated: true, completion: nil)
    //    }
    
    
    
    
    func openDocumentMenu() {
        let types = [kUTTypeMP3, kUTTypeAudio, kUTTypeMPEG].map({String($0)})
        let vc = UIDocumentPickerViewController.init(documentTypes: types, in: UIDocumentPickerMode.open)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension CastVC : UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {return}
        //
        //        self.convertAudioCAF(url: url)
        
        url.startAccessingSecurityScopedResource()
        var error : NSError?
        let coordinator = NSFileCoordinator.init()
        coordinator.coordinate(readingItemAt: url, options: .immediatelyAvailableMetadataOnly, error: &error) { (coordinatorUrl) in
            self.getMediaItemURL(assetURL: coordinatorUrl)
        }
    }
}

//MARK: - UICollectionView
extension CastVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = dataSource[indexPath.row]
        
        if item.rawValue == CastType.native_ads.rawValue {
            let cell = collectionView.dequeueReusableCell(withClass: NativeAdsCLCell.self, for: indexPath)
            cell.parentView = self
            cell.fillData()
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withClass: CastItemCLCell.self, for: indexPath)
            cell.fillData(item)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = dataSource[indexPath.row]
        if item.rawValue == CastType.native_ads.rawValue {
            return
        }
        switch item {
        case .photo:
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            present(picker, animated: true)
            break
        case .video:
            openPicker(kUTTypeMovie as String)
            break
        case .youtube:
            if Configuration.inPremiumUser() == true {
                let vc = YTSearchVC()
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                InAppPerchaseManager.shared.openGetPremium(self)
            }
            break
        case .sound:
            showAlert(title: "", message: "", buttonTitles: ["iCloud File", "Library", "Cancel"], highlightedButtonIndex: nil) { (index) in
                if index == 0 {
                    self.openDocumentMenu()
                } else if index == 1 {
                    self.fireMediaPicker()
                }
            }
            break
        case .mirroring:
            if Configuration.inPremiumUser() == true {
                //TODO
                recorder()
            }else {
                InAppPerchaseManager.shared.openGetPremium(self)
            }
            break
        default:
            self.showAlert(title: kComingSoon, message: nil)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = dataSource[indexPath.row]
        
        
        let column = 2
        let width = (collectionView.bounds.width - CGFloat((column + 1)) * space) / CGFloat(column)
        if item.rawValue == CastType.native_ads.rawValue {
            return CGSize.init(width: width, height: width * 3/4)
            
        }
        return CGSize.init(width: width, height: width * 3/4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        space
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: space, left: space, bottom: space, right: space)
    }
}
//MARK: - UIImagePickerController
extension CastVC {
    @objc func openPicker(_ mediaTypes : String) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = false
        pickerController.mediaTypes = [mediaTypes]
        pickerController.sourceType = .photoLibrary
        present(pickerController, animated: true, completion: nil)
    }
    
    
    @objc func fireMediaPicker() {
        if mediaPickerController == nil {
            mediaPickerController = MPMediaPickerController(mediaTypes: .anyAudio)
            //mediPic_VC = MPMediaPickerController(mediaTypes: .music)
            mediaPickerController?.delegate = self
        }
        
        self.present(mediaPickerController!, animated: true, completion: nil)
    }
}

extension CastVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: {
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                self.generateGCDWebServerDataResponse(url: url)
            } else if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                self.generateGCDWebServerDataResponse(url: url)
            }
        })
        
        
    }
}
extension CastVC : MPMediaPickerControllerDelegate {
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        mediaPicker.dismiss(animated: true, completion: nil)
        guard let mediaItem = mediaItemCollection.items.first else {
            return
        }
        getMediaItemURL(assetURL: mediaItem.assetURL)
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true, completion: nil)
    }
    
    func getMediaItemURL(assetURL : URL?) {
        
        guard let url = assetURL else {return}
        let songAsset = AVURLAsset(url: url)
        guard let exporter = AVAssetExportSession(asset: songAsset, presetName: AVAssetExportPresetAppleM4A) else {
            assertionFailure()
            return
        }
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {return}
        exporter.outputFileType = .m4a
        let fileName = "\(Date().nanosecond).m4a"
        let fileURL = path.appendingPathComponent(fileName)
        let outputURL = URL.init(fileURLWithPath: fileURL)
        exporter.outputURL = outputURL
        // do the export
        exporter.exportAsynchronously {
            let status = exporter.status
            switch status {
            case .failed:
                print(exporter.error)
            case .completed:
                DispatchQueue.main.async {
                    self.generateGCDWebServerDataResponse(url: outputURL)
                }
            default:
                break
            }
        }
    }
}



extension CastVC: RPBroadcastActivityViewControllerDelegate {
    func broadcastActivityViewController(_ broadcastActivityViewController: RPBroadcastActivityViewController, didFinishWith broadcastController: RPBroadcastController?, error: Error?) {
        self.broadcastController = broadcastController
        self.broadcastController?.delegate = self
        broadcastActivityViewController.dismiss(animated: true) {
            self.broadcastController?.startBroadcast { [unowned self] error in
                // broadcast started
                print("broadcast started with error: \(String(describing: error))")
                
                if let cameraPreviewView = RPScreenRecorder.shared().cameraPreviewView {
                    cameraPreviewView.frame = CGRect(x: 0, y: self.topLayoutGuide.length, width: 200, height: 200)
                    self.view.addSubview(cameraPreviewView)
                }
            }
        }
    }
}

extension CastVC: RPBroadcastControllerDelegate {
    func broadcastController(_ broadcastController: RPBroadcastController, didFinishWithError error: Error?) {
        print("broadcast did finish with error: \(String(describing: error))")
    }
    
    func broadcastController(_ broadcastController: RPBroadcastController, didUpdateServiceInfo serviceInfo: [String : NSCoding & NSObjectProtocol]) {
        print("broadcast did update service info: \(serviceInfo)")
    }
    
    func broadcastController(_ broadcastController: RPBroadcastController, didUpdateBroadcast broadcastURL: URL) {
        print("broadcast did update URL: \(broadcastURL)")
    }
}

extension CastVC {
    @objc func recorder() {
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? ""
        let preferredExtension = "\(bundleIdentifier).Extension"
        let broadcastPickerView = RPSystemBroadcastPickerView.init()
        
        view.addSubview(broadcastPickerView)
        broadcastPickerView.preferredExtension = preferredExtension
        broadcastPickerView.backgroundColor = .clear
        broadcastPickerView.showsMicrophoneButton = true
        
        if let button = broadcastPickerView.subviews.first as? UIButton {
            button.sendActions(for: .touchUpInside)
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                self.checkBroadCast()
            }
        }
    }
    
    @objc func didReceiveBroadcast(_ notification: NSNotification) {
        if let url = notification.userInfo?["url"] as? URL {
            self.generateGCDWebServerDataResponse(url: url)
        }
    }
    
    func checkBroadCast() {
        if let value = userDefaults?.bool(forKey: kBroadcastStarted) {
            if value {
                let mediaURL = URL.init(string: "http://\(self.getIPAddress()):8080/hello/playlist.m3u8")!
//                self.streamMirror(mediaURL, contentType: nil)
                userDefaults?.set(false, forKey: kBroadcastStarted)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                    self.streamMirror(mediaURL, contentType: nil)
//                }
                //                self.streamMirror(mediaURL, contentType: nil)
            } else {
                
            }
        }
    }
    
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]
                    
                    let name: String = String(cString: (interface.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
}
