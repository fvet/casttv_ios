//
//  Constant.swift
//  CastTV
//
//  Created by Kiu on 7/24/21.
//

import Foundation
import UIKit

let NAVI_COLOR = UIColor.init(hexString: "202020")!
let TINT_COLOR = UIColor.init(hexString: "0094FF")!
//MARK: - FONTS

let FONT_DEFAULT            =  "HelveticaNeue"
let FONT_BOLD               =  "HelveticaNeue-Bold"
let FONT_BOLD_ITALIC        =  "HelveticaNeue-BoldItalic"
let FONT_ITALIC             =  "HelveticaNeue-Italic"
let FONT_LIGHT              =  "HelveticaNeue-Light"
let FONT_LIGHT_ITALIC       =  "HelveticaNeue-LightItalic"
let FONT_MEDIUM             =  "HelveticaNeue-Medium"
let FONT_MEDIUM_ITALIC      =  "HelveticaNeue-MediumItalic"
let FONT_REGULAR            =  "HelveticaNeue"
let FONT_THIN               =  "HelveticaNeue-Thin"
let FONT_THIN_ITALIC        =  "HelveticaNeue-ThinItalic"
let FONT_ULTRA_LIGHT        =  "HelveticaNeue-UltraLight"
let FONT_ULTRA_LIGHT_ITALIC =  "HelveticaNeue-UltraLightItalic"
let FONT_COND_BLACK         =  "HelveticaNeue-CondensedBlack"
let FONT_COND_BOLD          =  "HelveticaNeue-CondensedBold"


let kComingSoon = "Coming soon!"
//let kNotificationBroadcast = NSNotification.Name(rawValue: "notificationName")
