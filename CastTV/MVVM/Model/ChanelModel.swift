//
//  ChanelModel.swift
//  CastTV
//
//  Created by Admin on 29/07/2021.
//

import Foundation
struct ChanelListModel : Codable {
    
    var result : [[ChanelModel]]?
    
    init(from decoder: Decoder) throws {
        result = try? decoder.decode("result")
    }
}
struct ChanelModel : Codable {
    
    var title : String?
    var uri : String?
    var icon : String?
    
    init(from decoder: Decoder) throws {
        title = try? decoder.decode("title")
        uri = try? decoder.decode("uri")
        icon = try? decoder.decode("icon")
    }
}
