//
//  YTSearchVC.swift
//  CastTV
//
//  Created by Kiu on 7/19/21.
//

import UIKit
import SwifterSwift

class YTSearchVC: BaseCastVC {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var tableView: UITableView! {
        didSet {
//            tableView.register(cellWithClass: YTVideoTBCell.self)
            tableView.register(nibWithCellClass: YTVideoTBCell.self)
            tableView.tableFooterView = UIView()
        }
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    //MARK: OTHER VARIABLES
    var viewModel = YTViewModel()
    var dataSources = [YTItem]()
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupVar()
        setupUI()
        callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
        
    }
    
    func setupUI() {
        title = "Youtube"
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    @objc func searchYT() {
        guard let keyword = searchBar.text, !keyword.isEmpty else {
            self.dataSources.removeAll()
            self.tableView.reloadData()
            return
        }
        viewModel.keyword = keyword
        viewModel.searchVideo { (result) in
            switch result {
            case .success(let data):
                self.dataSources = data.items ?? []
                self.tableView.reloadData()
            default:
                break
            }
        }
    }
    
    func getLink(_ model : YTItem) {
        guard let videoId = model.id?.videoId else {return}
        viewModel.youtubeURL = "https://www.youtube.com/watch?v=\(videoId)"
        viewModel.getMP4Link { (result) in
            switch result {
            case .success(let model):
                guard let url = model.url?.first(where: { (url) -> Bool in
                    return url.name?.contains("MP4") == true
                })?.url else {return}
                guard let castURL = URL.init(string: url) else {return}
                super.streamVideo(castURL, contentType: "video/mp4")
                break
            default:
                break
            }
        }
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    
}

extension YTSearchVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: YTVideoTBCell.self, for: indexPath)
        let item = dataSources[indexPath.row]
        cell.fillData(item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = dataSources[indexPath.row]
        getLink(item)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension YTSearchVC : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
//        searchYT()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(searchYT), object: nil)
        self.perform(#selector(searchYT), with: self, afterDelay: 0.3)
//        searchYT()
    }
}
