//
//  SettingNativeAdsTBCell.swift
//  CastTV
//
//  Created by Admin on 29/07/2021.
//

import UIKit
import GoogleMobileAds

class SettingNativeAdsTBCell: UITableViewCell {
    var adLoader: GADAdLoader!
    var parentView: UIViewController?

    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var mediaView: GADMediaView!
    @IBOutlet weak var viewAds: GADNativeAdView!
    @IBOutlet weak var lbAds: UILabel!
    @IBOutlet weak var lbHeadline: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
 
    }
    func fillData() {
        guard let vc = self.parentView else {
            return
        }
        adLoader = GADAdLoader(
          adUnitID: IDNativeAds, rootViewController: vc,
          adTypes: [.native], options: nil)
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }

    
    
}

extension SettingNativeAdsTBCell: GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print(error)
    }
    
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        lbHeadline.text = nativeAd.headline
        
        mediaView.mediaContent = nativeAd.mediaContent
        // showing or hiding them.
        lbAds.text = nativeAd.advertiser
        lbAds.isHidden = nativeAd.advertiser == nil

        btnAction.setTitle(nativeAd.callToAction, for: .normal)
        btnAction.isHidden = nativeAd.callToAction == nil

      }
}
