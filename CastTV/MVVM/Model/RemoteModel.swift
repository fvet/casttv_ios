//
//  RemoteModel.swift
//  CastTV
//
//  Created by Admin on 26/07/2021.
//

import Foundation
struct RemoteModel : Codable {
    
    var result : Bool?
    var response : String?
    var token : String?
    
    init(from decoder: Decoder) throws {
        result = try? decoder.decode("result")
        response = try? decoder.decode("response")
        token = try? decoder.decode("token")
    }
}
struct TVModelResponse : Codable {

        var id : Int?
        var result : [ResultModel]?

        init(from decoder: Decoder) throws {
            id = try? decoder.decode("id")
            result = try? decoder.decode("result")
        }

 /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if result != nil {
            var dictionaryElements = [[String:Any]]()
            for resultElement in result! {
                dictionaryElements.append(resultElement.toDictionary())
            }
            dictionary["result"] = dictionaryElements
        }
        return dictionary
    }
}

struct ResultModel : Codable {

        var status : String?

        init(from decoder: Decoder) throws {
            status = try? decoder.decode("status")
        }

 /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
}











