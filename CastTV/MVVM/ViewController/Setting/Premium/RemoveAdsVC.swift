//
//  PremiumVC.swift
//  CastTV
//
//  Created by Kiu on 7/28/21.
//

import UIKit
import SwiftyStoreKit

class RemoveAdsVC: UIViewController {
    
    //MARK: IBOUTLETS
    
    @IBOutlet weak var btnRemove: UIButton!
    //MARK: OTHER VARIABLES
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupVar()
        setupUI()
        callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
        SHARE_APPLICATION_DELEGATE.showLoading()
        SwiftyStoreKit.retrieveProductsInfo([ProductID.remove_ads.rawValue]) { (result) in
            SHARE_APPLICATION_DELEGATE.hideLoading()
            result.retrievedProducts.forEach { (product) in
                let priceString = product.localizedPrice!
                if product.productIdentifier == ProductID.remove_ads.rawValue {
                    self.btnRemove.setTitle("Remove ADS (\(priceString))", for: .normal)
                }
            }
        }
    }
    
    func setupUI() {
        
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    func remove_ads() {
        self.subscriptionAction(productId: ProductID.remove_ads.rawValue)
    }
    
    func subscriptionAction(productId: String) {
        SHARE_APPLICATION_DELEGATE.showLoading()
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { [weak self] (result) in
            SHARE_APPLICATION_DELEGATE.hideLoading()
            guard let `self` = self else { return }
            //self.hideLoading()
            switch result {
            case .success(_):
                Configuration.joinRemoveAdsUser(join: true)
                self.showAlert(title: "Successful", message: nil, buttonTitles: ["OK"], highlightedButtonIndex: nil) { [weak self] (_) in
                    SHARE_APPLICATION_DELEGATE.setupRootView()
                }
            case .error(_):
                self.showAlert(title: "Cannot subcribe", message: "Cannot subcribe")
                break
            }
        }
    }
    //MARK: - BUTTON ACTIONS
    @IBAction func didTouchClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func removeAds(_ sender: Any) {
        remove_ads()
    }
    
    
}
