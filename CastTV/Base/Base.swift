//
//  Base.swift
//  DemoApp
//
//  Created by Kiu on 7/8/21.
//

import Foundation
import UIKit

class BaseVC: UIViewController {

    //MARK: IBOUTLETS
    
    //MARK: OTHER VARIABLES
    

    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
            setupVar()
            setupUI()
            callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
        
    }
    
    func setupUI() {
    
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS

}
