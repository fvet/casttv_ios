//
//  ParserVideo.swift
//  DemoApp
//
//  Created by Kiu on 7/19/21.
//

import Foundation

struct ParserVideo : Codable {
    
    var cipher : Bool?
    //        var converter : Converter?
    var diffConverter : String?
    //        var hd : AnyObject?
    var hosting : Int?
    var id : String?
    var itags : [String]?
    var meta : Meta?
    //        var sd : AnyObject?
    var srv : String?
    var thumb : String?
    var url : [Url]?
    var video_quality : [String]?
    
    init(from decoder: Decoder) throws {
        cipher = try? decoder.decode("cipher")
        //            converter = try? decoder.decode("converter")
        diffConverter = try? decoder.decode("diffConverter")
        //            hd = try? decoder.decode("hd")
        hosting = try? decoder.decode("hosting")
        id = try? decoder.decode("id")
        itags = try? decoder.decode("itags")
        meta = try? decoder.decode("meta")
        //            sd = try? decoder.decode("sd")
        srv = try? decoder.decode("srv")
        thumb = try? decoder.decode("thumb")
        url = try? decoder.decode("url")
        video_quality = try? decoder.decode("video_quality")
    }
}
struct Url : Codable {
    
    //        var attr : Attr?
    var audio : Bool?
    var downloadable : Bool?
    var ext : String?
    var filesize : Int?
    var group : String?
    var info_token : String?
    var info_url : String?
    var isConverter : Bool?
    var itag : String?
    var name : String?
    var no_audio : Bool?
    var quality : String?
    var subname : String?
    var type : String?
    var url : String?
    
    init(from decoder: Decoder) throws {
        //            attr = try? decoder.decode("attr")
        audio = try? decoder.decode("audio")
        downloadable = try? decoder.decode("downloadable")
        ext = try? decoder.decode("ext")
        filesize = try? decoder.decode("filesize")
        group = try? decoder.decode("group")
        info_token = try? decoder.decode("info_token")
        info_url = try? decoder.decode("info_url")
        isConverter = try? decoder.decode("isConverter")
        itag = try? decoder.decode("itag")
        name = try? decoder.decode("name")
        no_audio = try? decoder.decode("no_audio")
        quality = try? decoder.decode("quality")
        subname = try? decoder.decode("subname")
        type = try? decoder.decode("type")
        url = try? decoder.decode("url")
    }
}


struct Meta : Codable {
    
    var duration : String?
    var source : String?
    //        var subtitle : Subtitle?
    var tags : String?
    var title : String?
    
    init(from decoder: Decoder) throws {
        duration = try? decoder.decode("duration")
        source = try? decoder.decode("source")
        //            subtitle = try? decoder.decode("subtitle")
        tags = try? decoder.decode("tags")
        title = try? decoder.decode("title")
    }
}
