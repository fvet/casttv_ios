//
//  BaseNavigationVC.swift
//  CastTV
//
//  Created by Kiu on 7/25/21.
//
import UIKit
import Foundation
import SwifterSwift
class BaseNavigationVC: UINavigationController, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupNavUI()
    }
    func setupNavUI() {
        self.delegate = self
        self.navigationBar.setColors(background: NAVI_COLOR, text: .white)
        // Changing the Font of Navigation Bar Title
//        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont(name: FONT_MEDIUM, size: 14)!, NSAttributedString.Key.foregroundColor : UIColor.white  ] as [NSAttributedString.Key : Any]
//        let image = UIImage.init(color: NAVI_COLOR, size: .zero).resizableImage(withCapInsets: .zero, resizingMode: .stretch)
//        self.navigationBar.setBackgroundImage(image, for: .default)
        // Changing the Color and Font of Back button
//        self.navigationBar.tintColor = UIColor.white
        
        //UINavigationBar .appearance().backgroundColor = UIColor.black
        
        //         Hidden title Back in UIBarButtonItem
        if #available(iOS 11, *) {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -300, vertical: 0), for:UIBarMetrics.default)
        } else {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -200), for:UIBarMetrics.default)
        }
        
        //        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "ic_back")
        //        UINavigationBar.appearance().backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "ic_back")
        
        
//        navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.layoutIfNeeded()
        
    }
}

class BaseViewController: UIViewController {
    
    var callBackAction: ((_ action: CALLBACK_ACTION?) -> ())?
    var callBackWithAction: ((_ action: Int?, _ value: Any?) -> ())?
    
    var callBackWithAction2: ((_ action: Int?, _ value: Any? , _ value: Any?) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
     override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
         self.navigationController?.isNavigationBarHidden = false
     }

}


class BaseHiddenNavigationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
     override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
         self.navigationController?.isNavigationBarHidden = false
     }
    
 
}

enum CALLBACK_ACTION {
    case ok
    case close
}


extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}
