//
//  SettingHeaderTBCell.swift
//  CastTV
//
//  Created by Kiu on 7/27/21.
//

import UIKit

class SettingHeaderTBCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
