//
//  RemoteConfigManager.swift
//  HelloSpa
//
//  Created by Dong Nguyen on 9/21/18.
//  Copyright © 2018 Dong Nguyen. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

enum RemoteConfigKey: String {
    case APP_STORE = "ios_app_store"
    case APP_VERSION = "ios_version"
    case IRONSOURCE_ID = "ironsource_id"
    case ADS_IS_ADMOD = "is_admod"
    case id_native_admob = "id_native_admob"
    case id_interstitial_admob = "id_interstitial_admob"
    case id_banner_admob = "id_banner_admob"
    case id_open_ads = "id_open_ads"
    case is_remove_ads = "is_remove_ads"

    }



struct RemoteConfigManager {
    static let APP_VERSION = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    static let APP_STORE = ""
    static let IRONSOURCE_ID = ""
    static let ADS_IS_ADMOD = ""
    static let id_native_admob = ""
    static let id_interstitial_admob = ""
    static let id_banner_admob = ""
    static let id_open_ads = ""
    static let is_remove_ads = ""

    static let shared = RemoteConfigManager()
    
    fileprivate var remoteConfig: RemoteConfig
    
    fileprivate init() {
        self.remoteConfig = RemoteConfig.remoteConfig()
        self.setDefaultValue()
    }
    
    func setDefaultValue() {
        let linkStore = RemoteConfigManager.APP_STORE as NSString
        let version = RemoteConfigManager.APP_VERSION as NSString
        let ironsource_id = RemoteConfigManager.IRONSOURCE_ID as NSString
        let isAdmod = RemoteConfigManager.ADS_IS_ADMOD as NSString
        let id_native_admob = RemoteConfigManager.id_native_admob as NSString
        let id_interstitial_admob = RemoteConfigManager.id_interstitial_admob as NSString
        let id_banner_admob = RemoteConfigManager.id_banner_admob as NSString
        let id_open_ads = RemoteConfigManager.id_open_ads as NSString
        let is_remove_ads = RemoteConfigManager.is_remove_ads as NSString

        remoteConfig.setDefaults([RemoteConfigKey.APP_STORE.rawValue : linkStore])
        remoteConfig.setDefaults([RemoteConfigKey.APP_VERSION.rawValue : version])
        remoteConfig.setDefaults([RemoteConfigKey.IRONSOURCE_ID.rawValue : ironsource_id])
        remoteConfig.setDefaults([RemoteConfigKey.ADS_IS_ADMOD.rawValue : isAdmod])
        remoteConfig.setDefaults([RemoteConfigKey.id_native_admob.rawValue : id_native_admob])
        remoteConfig.setDefaults([RemoteConfigKey.id_interstitial_admob.rawValue : id_interstitial_admob])
        remoteConfig.setDefaults([RemoteConfigKey.id_banner_admob.rawValue : id_banner_admob])
        remoteConfig.setDefaults([RemoteConfigKey.id_open_ads.rawValue : id_open_ads])
        remoteConfig.setDefaults([RemoteConfigKey.is_remove_ads.rawValue : is_remove_ads])

    }
    
    
    func getValue(fromKey key: RemoteConfigKey) -> String {
        if let value = remoteConfig[key.rawValue].stringValue {
            return value
        }
        return ""
    }
    
    
    func fetchRemoteConfig(_ completeHander: ((_ status : RemoteConfigFetchStatus)->())?) {
        let expirationDuration: TimeInterval
        #if DEBUG
        expirationDuration = 0
        #else
        expirationDuration = 3600
        #endif
        remoteConfig.fetch(withExpirationDuration: expirationDuration) { (status, error) in
            print("remoteConfig.fetch error \(error)")
            if status == .success {
                self.remoteConfig.activate { (error,arg)  in
                    print("remoteConfig.activate error \(error)")
//                    SHARE_APPLICATION_DELEGATE.checkAppVersion()
                }
            }
            if completeHander != nil {
                completeHander!(status)
            }
        }
    }
    
}


/// APIManager for Siren
public struct APIManager {
    /// Constants used in the `APIManager`.
    private struct Constants {
        /// Constant for the `bundleId` parameter in the iTunes Lookup API request.
        static let bundleID = "bundleId"
        /// Constant for the `country` parameter in the iTunes Lookup API request.
        static let country = "country"
    }
    
    ///
    let countryCode: String?
    
    /// Initializes `APIManager` to the region or country of an App Store in which the app is available.
    /// By default, all version check requests are performed against the US App Store.
    /// If the app is not available in the US App Store, set it to the identifier of at least one App Store region within which it is available.
    ///
    /// [List of country codes](https://help.apple.com/app-store-connect/#/dev997f9cf7c)
    ///
    /// - Parameter countryCode: The country code for the App Store in which the app is availabe. Defaults to nil (e.g., the US App Store)
    public init(countryCode: String? = nil) {
        self.countryCode = countryCode
    }
    
    /// The default `APIManager`.
    ///
    /// The version check is performed against the  US App Store.
    public static let `default` = APIManager()
}

extension APIManager {
    /// Creates and performs a URLRequest against the iTunes Lookup API.
    ///
    /// Creates the URL that points to the iTunes Lookup API.
    ///
    /// - Returns: The iTunes Lookup API URL.
    /// - Throws: An error if the URL cannot be created.
    func makeITunesURL() -> URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "itunes.apple.com"
        components.path = "/lookup"
        
        var items: [URLQueryItem] = [URLQueryItem(name: Constants.bundleID, value: Bundle.main.bundleIdentifier)]
        
        if let countryCode = countryCode {
            let item = URLQueryItem(name: Constants.country, value: countryCode)
            items.append(item)
        }
        
        components.queryItems = items
        
        guard let url = components.url, !url.absoluteString.isEmpty else {
            return nil
        }
        
        return url
    }
}
