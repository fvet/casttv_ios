//
//  SettingVC.swift
//  CastTV
//
//  Created by Kiu on 7/27/21.
//

import UIKit
import GoogleMobileAds
import StoreKit
import MessageUI
import SwiftyStoreKit
enum SettingType : String {
    case removeAds = "Remove Ads"
    case findRemote = "Find Remote"
    case share = "Share"
    case rate = "Rate your App"
    case feedBack = "Feedback"
    case restore = "Restore"

    func getIcon() -> UIImage {
        switch self {
        case .findRemote:
            return #imageLiteral(resourceName: "ic_find_remote")
        case .removeAds:
            return #imageLiteral(resourceName: "remove_ads")
        case .share:
            return #imageLiteral(resourceName: "ic_share")
        case .rate:
            return #imageLiteral(resourceName: "ic_rate")
        case .feedBack:
            return #imageLiteral(resourceName: "ic_feedback")
        case .restore:
            return #imageLiteral(resourceName: "restore")
        }
    }
}

class SettingVC: UIViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var contraintHeader: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIView!
    var titles = ["General", "Contact"]
    var dataSource : [[SettingType]] = [[.findRemote,.restore,.removeAds],
                                        [.rate, .feedBack, .share]]
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(nibWithCellClass: SettingTBCell.self)
            tableView.register(nibWithCellClass: SettingHeaderTBCell.self)
            tableView.register(nibWithCellClass: SettingNativeAdsTBCell.self)

            tableView.tableFooterView = UIView()
        }
    }
    @IBOutlet weak var contraintHeightAds: NSLayoutConstraint!
    @IBOutlet weak var viewAddBanner: GADBannerView!
    @IBOutlet weak var viewAddBannerIron: UIView!

    //MARK: OTHER VARIABLES
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupVar()
        setupUI()
        callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
        if Configuration.inRemoveAdsUser() == true || Configuration.inPremiumUser() == true {
            self.dataSource = [[.findRemote, .share],
                               [.rate, .feedBack]]
            self.tableView.reloadData()
            self.contraintHeightAds.constant = 0
            viewAddBanner.isHidden = true
            viewAddBannerIron.isHidden = true

            if Configuration.inPremiumUser() == true {
                self.contraintHeader.constant = 0
                viewHeader.isHidden = true
            }else {
                self.contraintHeader.constant = 50
                viewHeader.isHidden = false
            }
        }else {
            viewHeader.isHidden = false
            self.contraintHeader.constant = 50

            if IS_REMOVE_ADS == true {
                self.contraintHeightAds.constant = 0
                viewAddBanner.isHidden = true
                viewAddBannerIron.isHidden = true

            }else {
                self.contraintHeightAds.constant = 50
                self.viewAddBanner.isHidden = !IS_ADS_IS_ADMOD
                self.viewAddBannerIron.isHidden = IS_ADS_IS_ADMOD
                if IS_ADS_IS_ADMOD {
                    GoogleAdsModel.sharedInstance.requestGADBannerView(self.viewAddBanner, viewController: self) { success in
                        //
                    }
                }
            }
        }
    }
    
    func setupUI() {
        navigationItem.title = "Setting"
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    @IBAction func gotoPremium(_ sender: Any) {
        if Configuration.inPremiumUser() == false{
            InAppPerchaseManager.shared.openGetPremium(self)
        }
    }
    
}

extension SettingVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: SettingTBCell.self, for: indexPath)
        cell.fillData(dataSource[indexPath.section][indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withClass: SettingHeaderTBCell.self)
        cell.lbTitle.text = titles[section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = dataSource[indexPath.section][indexPath.row]
        switch item {
        case .findRemote:
            if let vc = SHARE_APPLICATION_DELEGATE.remoteVC as RemoteTVVC? {
                SHARE_APPLICATION_DELEGATE.tabbarVC?.selectedIndex = 0
                vc.startNewTV()
            }
        case .share:
            let items = [URL(string: "https://www.apple.com")!]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            present(ac, animated: true)
            break
        case .rate:
            self.rateApp()
            break
        case .feedBack:
            self.sendEmail()
            break
        case .removeAds:
            if Configuration.inRemoveAdsUser() == false{
                let vc = RemoveAdsVC()
                vc.modalPresentationStyle = .overFullScreen
                present(vc, animated: true, completion: nil)

            }
            break
        case .restore:
            SHARE_APPLICATION_DELEGATE.showLoading()
            SwiftyStoreKit.restorePurchases { results in
                SHARE_APPLICATION_DELEGATE.hideLoading()
                if results.restoredPurchases.count == 0 {
                    self.showAlert(title: "", message: "Cannot restore")
                    return
                }
                for product in results.restoredPurchases {
                    if product.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(product.transaction)
                    }
                    
                    if product.productId == ProductID.yearly.rawValue || product.productId == ProductID.monthly.rawValue || product.productId == ProductID.oneTime.rawValue {
                        print("Premium is restored")
                        Configuration.joinPremiumUser(join: true)
                        self.showAlert(title: "Successful", message: "Premium is restored", buttonTitles: ["OK"], highlightedButtonIndex: nil) { [weak self] (_) in
                            SHARE_APPLICATION_DELEGATE.setupRootView()
                        }
                    }
                    
                    if product.productId == ProductID.remove_ads.rawValue {
                        print(ProductID.remove_ads.rawValue)
                        Configuration.joinRemoveAdsUser(join: true)
                        self.showAlert(title: "Successful", message: "Remove ads is restored", buttonTitles: ["OK"], highlightedButtonIndex: nil) { [weak self] (_) in
                            SHARE_APPLICATION_DELEGATE.setupRootView()
                        }
                    }
                }
            }
            break
        }
    }
    
    func rateApp() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "id1581765635") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    
}
extension SettingVC : MFMailComposeViewControllerDelegate {
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["bosscuoi9x@gmail.com"])
            mail.setMessageBody("<p>If you have any questions about the app, please send it here:</p>", isHTML: true)

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

