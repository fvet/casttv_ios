//
//  StringExtension.swift
//  CastTV
//
//  Created by Kiu on 8/1/21.
//

import Foundation
extension String {
    var youtubeURL: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        let id = (self as NSString).substring(with: result.range)
        return "<iframe margin='0' padding='0' width='100%' height='100%' src='https://www.youtube.com/embed/\(id)?rel=0' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>"
        
    }
    
    var vimeoURL: String? {
        let pattern = "([0-9]+)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        
        let id  = (self as NSString).substring(with: result.range)
        return "<iframe src='https://player.vimeo.com/video/\(id)' width='100%' height='100%' frameborder='0' allow='autoplay; fullscreen' allowfullscreen></iframe>"
    }
    
    func isValidRegex(_ regex : String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        let result = predicate.evaluate(with: self) as Bool
        return result
    }
    
}
