//
//  BaseCastVC.swift
//  CastTV
//
//  Created by Kiu on 7/19/21.
//

import UIKit
import GoogleCast
import SnapKit

class BaseCastVC: UIViewController, GCKUICastButtonDelegate {
    
    var sessionManager: GCKSessionManager!
    var gcdWebServer : GCDWebServer?
    let notConnectVC = NotConnectVC()
    var isCast = false
    var castButton: GCKUICastButton!
    var mediaInformation : GCKMediaInformation?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkCastStateUI(isConnected: self.isCast)
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sessionManager = GCKCastContext.sharedInstance().sessionManager
        sessionManager.add(self)
        gcdWebServer = GCDWebServer()
        // Do any additional setup after loading the view.
        self.castButton = GCKUICastButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        //castButton.tintColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: castButton)
        notConnectVC.callBackAction = { [weak self] (_) in
            self?.castButton.sendActions(for: .touchUpInside)
        }
        
    }
    
    
    func streamVideo(_ mediaURL : URL, contentType : String?) {
        
        let mediaInfoBuilder = GCKMediaInformationBuilder.init(contentURL: mediaURL)
        mediaInfoBuilder.streamType = GCKMediaStreamType.none;
        mediaInfoBuilder.contentType = contentType ?? mediaURL.mimeType()
        //        mediaInfoBuilder.metadata = metadata; \\(providing metadata)
        let mediaInformation = mediaInfoBuilder.build()
        // Now finally handing over all information and load video
        if let request = sessionManager.currentSession?.remoteMediaClient?.loadMedia(mediaInformation){
            request.delegate = self
            GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
        }
        
    }
    
    func streamMirror(_ mediaURL : URL, contentType : String?) {
        //        GCKMediaInformationBuilder(mediaInformation: GCKMediaInformation.init())
        print(#function)
        print(mediaURL)
        let mediaInfoBuilder = GCKMediaInformationBuilder.init(contentURL: mediaURL)
        mediaInfoBuilder.contentID = mediaURL.absoluteString
        mediaInfoBuilder.contentType = mediaURL.mimeType()
        mediaInfoBuilder.streamType = GCKMediaStreamType.buffered
        mediaInfoBuilder.hlsSegmentFormat = .TS_AAC
        mediaInfoBuilder.hlsVideoSegmentFormat = .FMP4
        mediaInfoBuilder.streamDuration = kGCKInvalidTimeInterval
        mediaInformation = mediaInfoBuilder.build()
        
        if let request = sessionManager.currentSession?.remoteMediaClient?.loadMedia(mediaInformation!){
            request.delegate = self
//            GCKCastContext.sharedInstance().presentDefaultExpandedMediaControls()
        }
        
    }
    
    func generateGCDWebServerDataResponse(url : URL) {
        guard let data = try? Data.init(contentsOf: url) else {return}
        let pathExtension = url.pathExtension
        let lastPathComponent = "\(Date().millisecond).\(pathExtension)"
        if gcdWebServer?.isRunning == true {
            gcdWebServer?.stop()
        }
        gcdWebServer?.addHandler(forMethod: "GET", path: "/\(lastPathComponent)", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse.init(data: data as Data, contentType: url.mimeType())
        })
        gcdWebServer?.start(withPort: 8080, bonjourName: "GCD Web Server")
        guard let serverURL = gcdWebServer?.serverURL else {return}
        let castURL = "\(serverURL)\(lastPathComponent)"
        if let mediaURL = URL.init(string: castURL) {
            self.streamVideo(mediaURL, contentType: nil)
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func checkCastStateUI(isConnected : Bool? = nil) {
        if self.isCast {
            if self.children.contains(notConnectVC) {
                notConnectVC.removeViewAndControllerFromParentViewController()
            }
        }else {
            
            let isConnected = isConnected ?? (sessionManager.currentSession != nil)
            if !isConnected {
                if !self.children.contains(notConnectVC) {
                    self.addChildViewController(notConnectVC, toContainerView: self.view)
                    notConnectVC.view.snp.makeConstraints { (make) in
                        make.leading.trailing.top.bottom.equalToSuperview()
                    }
                }
            } else {
                if self.children.contains(notConnectVC) {
                    notConnectVC.removeViewAndControllerFromParentViewController()
                }
            }
        }
    }
}

extension BaseCastVC : GCKRequestDelegate {
    func requestDidComplete(_ request: GCKRequest) {
        print(#function)
        print(request)
    }
    
    func request(_ request: GCKRequest, didFailWithError error: GCKError) {
        print(#function)
        print(error)
    }
    
    func request(_ request: GCKRequest, didAbortWith abortReason: GCKRequestAbortReason) {
        print(#function)
        print(abortReason)
    }
}

extension BaseCastVC : GCKSessionManagerListener {
    func sessionManager(_ sessionManager: GCKSessionManager, didStart session: GCKSession) {
        let json = [
            "address_ip" : session.device.networkAddress.ipAddress ?? "",
            "pass_tv" : "123456",
            "name_tv" : session.device.modelName ?? ""
            
        ] as [String : Any]
        if let model = json.toCodableObject() as TVModel? {
            var token = Token()
            token.currentTV = model
        }
        checkCastStateUI()
        if let remove = SHARE_APPLICATION_DELEGATE.remoteVC {
            remove.connectTV()
        }
        if let channel = SHARE_APPLICATION_DELEGATE.channel {
            channel.callAPI()
        }
    }
    
    func sessionManager(_ sessionManager: GCKSessionManager, didEnd session: GCKSession, withError error: Error?) {
        checkCastStateUI()
    }
}


