//
//  CastItemsVC.swift
//  DemoApp
//
//  Created by Kiu on 7/11/21.
//

import UIKit
import Photos


//GCDWebServer* webServer = [[GCDWebServer alloc] init];
//[webServer addGETHandlerForBasePath:@"/" directoryPath:[[NSBundle mainBundle] bundlePath] indexFilename:nil cacheAge:3600 allowRangeRequests:YES];
//[webServer runWithPort:8080];

class CastItemsVC: UITableViewController {
//    var picker : UIImagePickerController!
//    var images = ["https://wallpaperdig.com/wp-content/uploads/2021/06/black-cool-hd-jeep-wallpaper.jpg",
//                  "https://wallpaperdig.com/wp-content/uploads/2021/06/snow-cool-hd-new-free-wallpapers.jpg",
//                  "https://wallpaperdig.com/wp-content/uploads/2021/05/hd-free-new-wallpapers-download.jpg",
//                  "https://wallpaperdig.com/wp-content/uploads/2021/05/toyota-hilux-hd-wallpaper-2048x1152.jpg"]
//    var videos = ["http://techslides.com/demos/sample-videos/small.mp4",
//                  "http://mirrors.standaloneinstaller.com/video-sample/jellyfish-25-mbps-hd-hevc.mp4",
//                  "http://mirrors.standaloneinstaller.com/video-sample/lion-sample.mp4"]
//    var device : ConnectableDevice?
//    var webServer : GCDWebServer?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Uncomment the following line to preserve selection between presentations
//        // self.clearsSelectionOnViewWillAppear = false
//
//        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        let barItem = UIBarButtonItem.init(title: "Fetch", style: .plain, target: self, action: #selector(openPicker))
//        self.navigationItem.rightBarButtonItem = barItem
//        webServer = GCDWebServer()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        device = SHARE_APPLICATION_DELEGATE.device
//    }
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 2
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        if section == 0 {
//            return images.count
//        } else {
//            return videos.count
//        }
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell: UITableViewCell = {
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
//                return UITableViewCell(style: .default, reuseIdentifier: "cell")
//            }
//            return cell
//        }()
//        if indexPath.section == 0 {
//            cell.textLabel?.text = images[indexPath.row]
//        } else {
//            cell.textLabel?.text = videos[indexPath.row]
//        }
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        var item = ""
//        if indexPath.section == 0 {
//            item = images[indexPath.row]
//            let url = URL.init(string: item)!
//            castImage(url: url)
//        } else {
//            item = videos[indexPath.row]
//            let url = URL.init(string: item)!
//            cast(url: url)
//        }
//    }
//
//    func cast(url : URL) {
//        let media = MediaInfo.init(url: url, mimeType: url.mimeType())
//        media?.title = ""
//        media?.description = ""
//        media?.addImage(ImageInfo.init(url: URL.init(string: "https://wallpaperdig.com/wp-content/uploads/2021/03/amazing-cars-download.jpg")!, type: UInt(ImageTypeThumb)))
//        //            device.mediaPlayer()?.displayImage(with: media, success: { (value) in
//        //                print(value)
//        //            }, failure: { (error) in
//        //                print(error)
//        //            })
//        device?.mediaPlayer()?.playMedia(with: media, shouldLoop: true, success: { (value) in
//            print(value)
//        }, failure: { (error) in
//            print(error)
//        })
//    }
//
//    func castImage(url : URL) {
//        let media = MediaInfo.init(url: url, mimeType: url.mimeType())
//        media?.title = ""
//        media?.description = ""
//        media?.addImage(ImageInfo.init(url: URL.init(string: "https://wallpaperdig.com/wp-content/uploads/2021/03/amazing-cars-download.jpg")!, type: UInt(ImageTypeThumb)))
//        //            device.mediaPlayer()?.displayImage(with: media, success: { (value) in
//        //                print(value)
//        //            }, failure: { (error) in
//        //                print(error)
//        //            })
//        device?.mediaPlayer()?.displayImage(with: media, success: { (value) in
//            print(value)
//        }, failure: { (error) in
//            print(error)
//        })
//
//    }
}

//extension CastItemsVC {
//    @objc func openPicker() {
//        let pickerController = UIImagePickerController()
//        pickerController.delegate = self
//        pickerController.allowsEditing = false
//        pickerController.mediaTypes = ["public.image", "public.movie"]
//        pickerController.sourceType = .photoLibrary
//        present(pickerController, animated: true, completion: nil)
//    }
//}
//
//extension CastItemsVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        var mediaURL : URL?
//        //        webServer?.stop()
//        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
//            //            let httpServer = HttpServer()
//            //            httpServer["/desktop/:path"] = shareFilesFromDirectory(url.path)
//            //            do {
//            //                try httpServer.start(9080, forceIPv4: true, priority: .background)
//            //                //                let myRequest = NSURLRequest(url: NSURL(string: ) as! URL)
//            //                castImage(url: URL.init(string: "http://localhost:9080")!)
//            //            } catch {
//            //                print(error)
//            //            }
//            if webServer?.isRunning == true {
//                webServer?.stop()
//            }
//            webServer?.addGETHandler(forPath: "/", filePath: url.path, isAttachment: true, cacheAge: 3600, allowRangeRequests: false)
//            webServer?.start(withPort: 8181, bonjourName: "")
//            if let serverURL = webServer?.serverURL {
//                castImage(url: serverURL)
//            }
//        } else if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
//            //            let httpServer = HttpServer()
//            //            httpServer["/desktop/:path"] = shareFilesFromDirectory(url.path)
//            //
//            //            do {
//            //                try httpServer.start(9080, forceIPv4: true, priority: .background)
//            //
//            //            } catch {
//            //                print(error)
//            //            }
//            if webServer?.isRunning == true {
//                webServer?.stop()
//            }
//            webServer?.addGETHandler(forPath: "/", filePath: url.path, isAttachment: true, cacheAge: 3600, allowRangeRequests: false)
////            webServer?.addGETHandler(forBasePath: "/\(url.lastPathComponent)", directoryPath: url.path, indexFilename: url.lastPathComponent, cacheAge: 3600, allowRangeRequests: true)
//            webServer?.start(withPort: 8181, bonjourName: "")
//            if let serverURL = webServer?.serverURL {
//                cast(url: serverURL)
//            }
//        }
//        
//        picker.dismiss(animated: true, completion: nil)
//    }
//}

// MARK:- Get File name from PHAsset.
extension PHAsset {
    
    var originalFilename: String? {
        
        var fname:String?
        
        if #available(iOS 9.0, *) {
            let resources = PHAssetResource.assetResources(for: self)
            if let resource = resources.first {
                fname = resource.originalFilename
            }
        }
        
        if fname == nil {
            // this is an undocumented workaround that works as of iOS 9.1
            fname = self.value(forKey: "filename") as? String
        }
        
        return fname
    }
}
