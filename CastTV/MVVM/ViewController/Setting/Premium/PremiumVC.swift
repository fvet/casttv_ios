//
//  PremiumVC.swift
//  CastTV
//
//  Created by Kiu on 7/28/21.
//

import UIKit
import SwiftyStoreKit

enum ProductID: String, CaseIterable {
    case yearly = "One_Yearly"
    case monthly = "One_Monthly"
    case oneTime = "One_Time_Payment"
    case remove_ads = "Remove_Ads"

}

enum ProductIDAutoRenew: String {
    case yearly = "One_Yearly"
    case monthly = "One_Monthly"
    case oneTime = "One_Time_Payment"
    case remove_ads = "Remove_Ads"
}

class PremiumVC: UIViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var lbPriceYear: UILabel!
    @IBOutlet weak var lbPriceMonth: UILabel!
    @IBOutlet weak var lbPriceOneTime: UILabel!

    //MARK: OTHER VARIABLES
    var inAppAutoRenew = false
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupVar()
        setupUI()
        callAPI()
    }
    
    //MARK: - SETUP UI & VAR
    func setupVar() {
        SHARE_APPLICATION_DELEGATE.showLoading()
        SwiftyStoreKit.retrieveProductsInfo([ProductID.yearly.rawValue, ProductID.monthly.rawValue, ProductID.oneTime.rawValue]) { (result) in
            SHARE_APPLICATION_DELEGATE.hideLoading()
            result.retrievedProducts.forEach { (product) in
                let priceString = product.localizedPrice!
                if product.productIdentifier == ProductID.yearly.rawValue {
                    self.lbPriceYear.text = "Then \(priceString) / Year after Trial ends (Best Popular)"
                } else if product.productIdentifier == ProductID.monthly.rawValue {
                    self.lbPriceMonth.text = "\(priceString) / Month (Best Flexible for any user)"
                }
                else if product.productIdentifier == ProductID.oneTime.rawValue {
                    self.lbPriceOneTime.text = "\(priceString) (Best Value)"
                }
            }
        }
    }
    
    func setupUI() {
        
    }
    
    //MARK - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    
    func monthlySubscription() {
        if inAppAutoRenew == true {
            self.subscriptionAction(productId: ProductIDAutoRenew.monthly.rawValue)
        } else {
            self.subscriptionAction(productId: ProductID.monthly.rawValue)
        }
    }
    
    func yearlySubscription() {
        
        if inAppAutoRenew == true {
            self.subscriptionAction(productId: ProductIDAutoRenew.yearly.rawValue)
        } else {
            self.subscriptionAction(productId: ProductID.yearly.rawValue)
        }
    }
    
    func oneTime() {
        
        if inAppAutoRenew == true {
            self.subscriptionAction(productId: ProductIDAutoRenew.oneTime.rawValue)
        } else {
            self.subscriptionAction(productId: ProductID.oneTime.rawValue)
        }
    }
    
    
    func remove_ads() {
        
        if inAppAutoRenew == true {
            self.subscriptionAction(productId: ProductIDAutoRenew.remove_ads.rawValue)
        } else {
            self.subscriptionAction(productId: ProductID.remove_ads.rawValue)
        }
    }
    
    func subscriptionAction(productId: String) {
        SHARE_APPLICATION_DELEGATE.showLoading()
        SwiftyStoreKit.purchaseProduct(productId, atomically: true) { [weak self] (result) in
            SHARE_APPLICATION_DELEGATE.hideLoading()
            guard let `self` = self else { return }
            switch result {
            case .success(_):
                Configuration.joinPremiumUser(join: true)
                if self.inAppAutoRenew {
                    Configuration.joinDiamondUser(join: true)
                }
                self.showAlert(title: "Successful", message: nil, buttonTitles: ["OK"], highlightedButtonIndex: nil) { [weak self] (_) in
                    SHARE_APPLICATION_DELEGATE.setupRootView()
                }
            case .error(_):
                self.showAlert(title: "Cannot subcribe", message: "Cannot subcribe")
                break
            }
        }
        
    }
    
    @IBAction func didTouchClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTouchMonth(_ sender: Any) {
        monthlySubscription()
    }
    
    @IBAction func didTouchYear(_ sender: Any) {
        yearlySubscription()
    }
    
    @IBAction func oneTime(_ sender: Any) {
        oneTime()
    }
}
