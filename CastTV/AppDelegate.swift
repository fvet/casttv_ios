//
//  AppDelegate.swift
//  DemoApp
//
//  Created by Kiu on 5/31/21.
//

import UIKit
import GoogleCast
import GoogleMobileAds
import SwiftyStoreKit
import IQKeyboardManagerSwift
import SVProgressHUD
import ObjectiveC.runtime
import Firebase
import FirebaseCrashlytics
import HaishinKit
import Logboard
import AVKit
let SHARE_APPLICATION_DELEGATE = UIApplication.shared.delegate as! AppDelegate
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    let logger = Logboard.with("com.haishinkit.Exsample.iOS")
    var inappManager = InAppPerchaseManager.shared
    //    let kReceiverAppID = "574C0882"//kGCKDefaultMediaReceiverApplicationID
    let kReceiverAppID = kGCKDefaultMediaReceiverApplicationID
    let kDebugLoggingEnabled = true
    var window: UIWindow?
    var remoteVC: RemoteTVVC?
    var channel: ChanelVC?
    var tabbarVC: BaseTabbarViewController?
    var appOpenAd: GADAppOpenAd?
    var loadTime = Date()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()

        activeAVAudioSession()
        let criteria = GCKDiscoveryCriteria(applicationID: kReceiverAppID)
        //            ** Following code enables CastConnect */
        
        
        let options = GCKCastOptions(discoveryCriteria: criteria)
        let launchOptions = GCKLaunchOptions()
        launchOptions.androidReceiverCompatible = true
        options.launchOptions = launchOptions
        GCKCastContext.setSharedInstanceWith(options)
        RemoteConfigManager.shared.fetchRemoteConfig { _ in
            if IS_REMOVE_ADS == true {
                GADMobileAds.sharedInstance().start(completionHandler: nil)
                IronSourceModel.sharedInstance.setupIronSourceSdk()
            }

            self.setupRootView()

        }

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        // Enable logger.
        GCKLogger.sharedInstance().delegate = self
        setupInAppPurchase()
        #if DEBUG
//        Configuration.resetData()
                Configuration.joinPremiumUser(join: true)
        #endif
//        Configuration.resetData()
//        MediaShareController.sharedInstance.searchServices()
        //        MediaShareController.sharedInstance.searchServices()
        
        
        
        return true
    }
    
    func requestAppOpenAd() {
        self.appOpenAd = nil
        GADAppOpenAd.load(withAdUnitID: IDOpenADs, request: GADRequest(), orientation: .portrait) { appOpenAd, error in
            if error != nil {
                print(error as Any)
                return
            }
            self.appOpenAd = appOpenAd
            self.appOpenAd!.fullScreenContentDelegate = self
            self.loadTime = Date()
            print("Ad is ready")
            
        }
    }
    
    func tryToPresentAd() {
        if let gOpenAd = self.appOpenAd, let rwc = self.window?.rootViewController, wasLoadTimeLessThanNHoursAgo(thresholdN: 4) {
                gOpenAd.present(fromRootViewController: rwc)
            } else {
                self.requestAppOpenAd()
            }
    }
    
    
    func wasLoadTimeLessThanNHoursAgo(thresholdN: Int) -> Bool {
        let now = Date()
        let timeIntervalBetweenNowAndLoadTime = now.timeIntervalSince(self.loadTime)
        let secondsPerHour = 3600.0
        let intervalInHours = timeIntervalBetweenNowAndLoadTime / secondsPerHour
        return intervalInHours < Double(thresholdN)
    }
    
    func setupRootView() {
        self.tabbarVC = BaseTabbarViewController()
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.rootViewController = self.tabbarVC!
        window?.makeKeyAndVisible()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        RemoteConfigManager.shared.fetchRemoteConfig { _ in
            if IS_REMOVE_ADS == false {
                if IS_ADS_IS_ADMOD == false {
                    self.tryToPresentAd()
                }
            }
        }
    }
    // MARK: UISceneSession Lifecycle
    //
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    
    func activeAVAudioSession() {
        let session = AVAudioSession.sharedInstance()
        do {
            if #available(iOS 10.0, *) {
                try session.setCategory(.playAndRecord, mode: .default, options: [.defaultToSpeaker, .allowBluetooth])
            } else {
                session.perform(NSSelectorFromString("setCategory:withOptions:error:"), with: AVAudioSession.Category.playAndRecord, with: [
                    AVAudioSession.CategoryOptions.allowBluetooth,
                    AVAudioSession.CategoryOptions.defaultToSpeaker
                ])
                try session.setMode(.default)
            }
            try session.setActive(true)
        } catch {
            logger.error(error)
        }
    }
}

extension AppDelegate : GCKLoggerDelegate {
    func logMessage(_ message: String, at level: GCKLoggerLevel, fromFunction function: String, location: String) {
        if (kDebugLoggingEnabled) {
            print(function + " - " + message)
        }
    }
}

extension AppDelegate {
    //MARK: - SETUP IN-APP PURCHASE
    func showLoading() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func setupInAppPurchase() {
        //fetch product
        SwiftyStoreKit.completeTransactions(atomically: true) { (purchases) in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    //Unlock content
                    print("productID " + purchase.productId)
                    
                case .failed, .purchasing, .deferred:
                    break
                @unknown default:
                    break
                }
            }
        }
    }
    
}

extension AppDelegate : GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("didFailToPresentFullScreenContentWithError")
        self.requestAppOpenAd()
    }
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("adDidPresentFullScreenContent")
    }
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("adDidDismissFullScreenContent")
        self.requestAppOpenAd()
    }
}
