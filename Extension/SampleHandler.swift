//
//  SampleHandler.swift
//  Extension
//
//  Created by Kiu on 8/18/21.
//

import ReplayKit
import HaishinKit
import VideoToolbox
import Logboard
import mamba
import Network

let logger = Logboard.with("com.haishinkit.Exsample.iOS.Screencast")

class SampleHandler: RPBroadcastSampleHandler {
    
    var httpStream : HTTPStream!
    var httpService : HLSService!
    let userDefaults = UserDefaults.init(suiteName: kAppGroup)
    override func broadcastStarted(withSetupInfo setupInfo: [String : NSObject]?) {
        // User has requested to start the broadcast. Setup info from the UI extension can be supplied but optional.
        Logboard.with(HaishinKitIdentifier).level = .trace
        
        httpStream = HTTPStream()
        httpStream.expectedMedias.insert(.video)
        httpStream.expectedMedias.insert(.audio)
        httpService = HLSService(domain: "", type: "_http._tcp", name: "Cast", port: 8080)
        httpService.addHTTPStream(httpStream)
        httpService.startRunning()
        httpStream.publish("hello")
        
        userDefaults?.set(true, forKey: kBroadcastStarted)
        userDefaults?.synchronize()
    }
    
    override func broadcastPaused() {
        // User has requested to pause the broadcast. Samples will stop being delivered.
    }
    
    override func broadcastResumed() {
        // User has requested to resume the broadcast. Samples delivery will resume.
    }
    
    override func broadcastFinished() {
        // User has requested to finish the broadcast.
        userDefaults?.set(false, forKey: kBroadcastStarted)
        userDefaults?.synchronize()
    }
    
    override func processSampleBuffer(_ sampleBuffer: CMSampleBuffer, with sampleBufferType: RPSampleBufferType) {
        
        switch sampleBufferType {
        case RPSampleBufferType.video:
            if let description:CMVideoFormatDescription = CMSampleBufferGetFormatDescription(sampleBuffer) {
                let dimensions:CMVideoDimensions = CMVideoFormatDescriptionGetDimensions(description)
                httpStream.videoSettings = [
                    .width: dimensions.width,
                    .height: dimensions.height ,
                    .profileLevel: kVTProfileLevel_H264_Baseline_AutoLevel,
                ]
            }
            self.httpStream.appendSampleBuffer(sampleBuffer, withType: .video)
            break
        case RPSampleBufferType.audioMic:
            self.httpStream.appendSampleBuffer(sampleBuffer, withType: .audio)
        default:
            break
        }
    }
}

