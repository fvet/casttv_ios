//
//  ChanelVC.swift
//  CastTV
//
//  Created by Admin on 29/07/2021.
//

import UIKit
import Kingfisher
import GoogleMobileAds

class ChanelVC: BaseCastVC {
    var dataSource:[ChanelModel] = [] {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.register(nibWithCellClass: ChanelCLCell.self)
            self.collectionView.reloadData()
        }
    }
    let space : CGFloat = 16
    @IBOutlet weak var viewAddBanner: GADBannerView!
    @IBOutlet weak var viewAddBannerIron: UIView!

    @IBOutlet weak var collectionView: UICollectionView!
    let viewModel = RemoteViewModel()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isCast = false
        SHARE_APPLICATION_DELEGATE.channel = self
        self.callAPI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Channels"
        self.setupVar()
        
    }
    
    func callAPI()  {
        self.dataSource = []
        self.checkCastStateUI(isConnected: false)
        self.viewModel.getApplicationList { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let model_response):
                if let datas = model_response.result?.first {
                    self.dataSource  = datas
                    self.checkCastStateUI(isConnected: true)
                }
                break
            default:
                self.checkCastStateUI(isConnected: false)
                break
            }
            
        }
    }
    
    
    func setupVar() {
        if Configuration.inPremiumUser() == true || Configuration.inRemoveAdsUser() == true || IS_REMOVE_ADS == true {
            viewAddBanner.isHidden = true
        }else {
            self.viewAddBanner.isHidden = !IS_ADS_IS_ADMOD
            self.viewAddBannerIron.isHidden = IS_ADS_IS_ADMOD

            if IS_ADS_IS_ADMOD {
                GoogleAdsModel.sharedInstance.requestGADBannerView(self.viewAddBanner, viewController: self) { success in
                    //
                }
            }
        }
    }
    
    func callOpenApp(_ model: ChanelModel) {
        self.viewModel.openApp(model.uri ?? "") { result in
            switch result {
            case .success:
                self.view.makeToast("Mở ứng dụng \(model.title ?? "") thành công")
                break
            default:
                self.view.makeToast("Mở ứng dụng \(model.title ?? "") Thất bại")
                break
            }
            
        }
    }


}
//MARK: - UICollectionView
extension ChanelVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: ChanelCLCell.self, for: indexPath)
        let item = dataSource[indexPath.row]
        cell.imgView?.kfImageURL(item.icon ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = dataSource[indexPath.row]
        self.callOpenApp(item)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let column = 3
        let width = (collectionView.bounds.width - CGFloat((column + 2)) * space) / CGFloat(column)
        return CGSize.init(width: width, height: width * 15/22)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: space, left: space, bottom: space, right: space)
    }
}
