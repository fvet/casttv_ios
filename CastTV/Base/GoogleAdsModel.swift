//
//  GoogleAdsModel.swift
//  iTopMucsic
//
//  Created by Hau Nguyen on 8/23/16.
//  Copyright © 2016 Hau Nguyen. All rights reserved.
//

import UIKit
import GoogleMobileAds

var APP_ADS_ID = "ca-app-pub-6084787406958367~8662582138"
var IDRewarded = "ca-app-pub-6084787406958367/7647640090"

var IDOpenADs :String {
    if let id_open_ads = RemoteConfigManager.shared.getValue(fromKey: .id_open_ads) as String? {
        if id_open_ads.count > 0{
            return id_open_ads
        }
    }
    return "ca-app-pub-3940256099942544/5662855259"
}

var IDBannerView :String {
    if let id_interstitial_admob = RemoteConfigManager.shared.getValue(fromKey: .id_banner_admob) as String? {
        if id_interstitial_admob.count > 0{
            return id_interstitial_admob
        }
    }
    return "ca-app-pub-6084787406958367/6643757609"
}

var IDInterstitial :String {
    if let id_interstitial_admob = RemoteConfigManager.shared.getValue(fromKey: .id_interstitial_admob) as String? {
        if id_interstitial_admob.count > 0{
            return id_interstitial_admob
        }
    }
    return "ca-app-pub-6084787406958367/8031068883"
}

var IDNativeAds :String {
    if let id_native_admob = RemoteConfigManager.shared.getValue(fromKey: .id_native_admob) as String? {
        if id_native_admob.count > 0{
            return id_native_admob
        }
    }
    return "ca-app-pub-6084787406958367/8467058784"
}
class GoogleAdsModel: NSObject , GADBannerViewDelegate {
    
    static let sharedInstance = GoogleAdsModel()
    var isLoadedBanner = false
    var bannerView: GADBannerView!
    var interstitial: GADInterstitialAd?
    var rewardedAd: GADRewardedAd?
    var Completed:((_ isLoadedBanner:Bool) -> Void)?
    var heightBanner = 0
    var countIndex = 0
    var RemovedAds = false
    var adRequestInProgress = false
    var adLoader: GADAdLoader!
    
    func initBannerView(_ frame:CGRect,viewController:UIViewController,completed:@escaping ((_ isLoadedBanner:Bool) -> Void)) {
        bannerView = GADBannerView.init(frame: frame)
        bannerView.adUnitID = IDBannerView
        bannerView.rootViewController = viewController
        let request = GADRequest()
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["35e3951a4a90a5e9b59fca09fdfbd83b"]
        // Request test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made.
        //        request.testDevices = [ kGADSimulatorID, "ca972684d3ff16e7763e7ba38faef507" ]
        bannerView.load(request)
        bannerView.delegate = self
        viewController.view.addSubview(bannerView)
        self.Completed = completed
    }
    
    func requestGADBannerView(_ banner:GADBannerView,viewController:UIViewController,completed:@escaping ((_ isLoadedBanner:Bool) -> Void)) {
        banner.adUnitID = IDBannerView
        banner.rootViewController = viewController
        let request = GADRequest()
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [kGADSimulatorID,"35e3951a4a90a5e9b59fca09fdfbd83b"]
        banner.load(request)
        banner.delegate = self
        self.Completed = completed
    }
    
    func initNativeAdView(_ frame:CGRect,viewController:UIViewController,completed:@escaping ((_ isLoadedBanner:Bool) -> Void)) {
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
        multipleAdsOptions.numberOfAds = 5
        adLoader = GADAdLoader(adUnitID: IDNativeAds, rootViewController: viewController,
                               adTypes: [.native],
                               options: [multipleAdsOptions])
        adLoader.delegate = self
        adLoader.load(GADRequest())
        self.Completed = completed
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView){
        isLoadedBanner = true
        Completed!(true)
    }
    
    func updateSizeTableView(_ completed:@escaping ((_ isLoadedBanner:Bool) -> Void)){
        self.Completed = completed
        Completed!(isLoadedBanner)
    }
    
    func updateFrameBanner(){
        var rectCurrent  = self.bannerView.frame
        rectCurrent.origin.y = rectCurrent.origin.y - 60
        self.bannerView.frame = rectCurrent
    }
    
    
    func createAndLoadVideoAds() {
        if rewardedAd != nil {
            self.showVideoAds(SHARE_APPLICATION_DELEGATE.window?.rootViewController)
        }else {
            let request = GADRequest()
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =  ["35e3951a4a90a5e9b59fca09fdfbd83b", "e9d0f7efb8249187d9edc781c8f89868"]
            GADRewardedAd.load(withAdUnitID: IDRewarded,
                               request: request, completionHandler: { (ad, error) in
                                if let error = error {
                                    print("Rewarded ad failed to load with error: \(error.localizedDescription)")
                                    return
                                }
                                self.rewardedAd = ad
                                self.rewardedAd?.fullScreenContentDelegate = self
                               }
            )
            
        }
    }
    
    func showVideoAds(_ viewController:UIViewController?) {
        if self.rewardedAd != nil {
            if let vc = viewController {
                vc.modalPresentationStyle = .fullScreen
                self.rewardedAd?.present(fromRootViewController: vc, userDidEarnRewardHandler: {
                })
            }
        }
    }
    
    func createAndLoadInterstitial(_ view: UIViewController) {
        let request = GADRequest()
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["35e3951a4a90a5e9b59fca09fdfbd83b", "e9d0f7efb8249187d9edc781c8f89868"]
        GADInterstitialAd.load(withAdUnitID:IDInterstitial,
                               request: request,
                               completionHandler: { (ad, error) in
                                if let error = error {
                                    print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                    return
                                }
                                self.interstitial = ad
                                self.interstitial!.fullScreenContentDelegate = self
                                self.showInterstitial(view)
                                })
    }
    
    func showInterstitialWhenUserAddedTrackToPlaylist(_ viewController:UIViewController){
        if interstitial != nil  {
            viewController.modalPresentationStyle = .fullScreen
            interstitial!.present(fromRootViewController: viewController)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    func showInterstitial(_ viewController:UIViewController){
        if interstitial != nil  {
            viewController.modalPresentationStyle = .fullScreen
            interstitial!.present(fromRootViewController: viewController)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    
    func interstitialDidReceiveAd(_ ad: GADInterstitialAd) {
        if let vc = SHARE_APPLICATION_DELEGATE.window?.rootViewController {
            vc.modalPresentationStyle = .fullScreen
            interstitial!.present(fromRootViewController:vc)
        }
    }
    
}

extension GoogleAdsModel: GADFullScreenContentDelegate {
    /// Tells the delegate that the rewarded ad was presented.
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Rewarded ad presented.")
    }
    /// Tells the delegate that the rewarded ad was dismissed.
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Rewarded ad dismissed.")
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "CloseAds")))
        countIndex = 0
        
    }
    /// Tells the delegate that the rewarded ad failed to present.
    func ad(_ ad: GADFullScreenPresentingAd,
            didFailToPresentFullScreenContentWithError error: Error) {
        print("Rewarded ad failed to present with error: \(error.localizedDescription).")
        isLoadedBanner = false
        Completed!(false)
        
    }
}
extension GoogleAdsModel: GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        Completed!(false)
        
    }
    
    func adLoader(_ adLoader: GADAdLoader,
                  didReceive nativeAd: GADNativeAd) {
        // A native ad has loaded, and can be displayed.
    }
    
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
        Completed!(true)
        
    }
}
