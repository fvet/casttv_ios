//
//  RemoteAPI.swift
//  CastTV
//
//  Created by Admin on 26/07/2021.
//

import Foundation
import Foundation
import Alamofire
import Codextended
let HOST = " https://bii-samsung-tv.herokuapp.com"
class RemoteAPI {
    func getApplicationList(url:String, _ params : [String : Any]?, _ complete: @escaping (Result<ChanelListModel, NetworkError>)->()) {
        
        RequestService.shared.requestWith(url, .post, params, ["Content-Type":"application/json",
                                                               "Connection": "keep-alive",
                                                               "Cookie":"auth=F82499835F670BE5A6AD71DABD3BB7467C6EE4FC",
                                                               "Content-Length":"5580"], objectType: ChanelListModel.self,encoding:JSONEncoding.default) { (result) in
            switch result {
            case .success(let data):
                guard let data = data as? ChanelListModel else {
                    complete(.failure(.decodingError))
                    return
                }
                complete(.success(data))
                break
            case .failure(let error):
                complete(.failure(error))
                break
            }
            
        }
    }
    func getPowerStatus(url:String, _ params : [String : Any]?, _ complete: @escaping (Result<TVModelResponse, NetworkError>)->()) {
        RequestService.shared.requestWith(url, .post, params, ["Content-Type":"application/json"], objectType: TVModelResponse.self,encoding:JSONEncoding.default) { (result) in
            switch result {
            case .success(let data):
                guard let data = data as? TVModelResponse else {
                    complete(.failure(.decodingError))
                    return
                }
                complete(.success(data))
                break
            case .failure(let error):
                complete(.failure(error))
                break
            }
            
        }
    }
    
    
    func getToken(_ params : [String : Any]?, _ complete: @escaping (Result<RemoteModel, NetworkError>)->()) {
        RequestService.shared.requestWith(HOST, .post, params, nil, objectType: RemoteModel.self) { (result) in
            switch result {
            case .success(let data):
                guard let data = data as? RemoteModel else {
                    complete(.failure(.decodingError))
                    return
                }
                complete(.success(data))
                break
            case .failure(let error):
                complete(.failure(error))
                break
            }
            
        }
    }
    
    func sendComandKey(_ params : [String : Any]?, _ complete: @escaping (Result<RemoteModel, NetworkError>)->()) {
        RequestService.shared.requestWith(HOST, .post, params, nil, objectType: RemoteModel.self) { (result) in
            switch result {
            case .success(let data):
                guard let data = data as? RemoteModel else {
                    complete(.failure(.decodingError))
                    return
                }
                complete(.success(data))
                break
            case .failure(let error):
                complete(.failure(error))
                break
            }
            
        }
    }
    
    
    
    
}
