//
//  YoutubeAPI.swift
//  DemoApp
//
//  Created by Kiu on 7/19/21.
//

import Foundation

let kAPIKEY = "AIzaSyCKgVG4J7wMFBZw0rWwJ4pTIQY4g8H3tu0"

class YoutubeAPI {
    
    
    
    func getMP4Link(params : [String : Any]?, _ complete: @escaping (Result<ParserVideo, NetworkError>)->()) {
        RequestService.shared.requestWith("https://api.y2mate.guru/api/convert", .post, params, nil, objectType: ParserVideo.self) { (result) in
            switch result {
            case .success(let data):
                guard let data = data as? ParserVideo else {
                    complete(.failure(.decodingError))
                    return
                }
                complete(.success(data))
                break
            case .failure(let error):
                complete(.failure(error))
                break
            }
            
        }
    }
    
    func searchVideo(params : [String : Any]?,  _ complete: @escaping (Result<SearchResponse, NetworkError>)->()) {
        
        
        RequestService.shared.requestWith("https://youtube.googleapis.com/youtube/v3/search", .get, params, nil, objectType: SearchResponse.self) { (result) in
            switch result {
            case .success(let data):
                guard let data = data as? SearchResponse else {
                    complete(.failure(.decodingError))
                    return
                }
                complete(.success(data))
                break
            case .failure(let error):
                complete(.failure(error))
                break
            }
            
        }
    }
    
}
