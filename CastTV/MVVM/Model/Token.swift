//
//  Token.swift
//  Kiple
//
//  Created by ThanhPham on 8/3/17.
//  Copyright © 2017 com.futurify.vn. All rights reserved.
//

import Foundation
struct TVModel : Codable {
    
    var address_ip : String?
    var name_tv : String?
    var pas_tv : String?
    
    
    init(from decoder: Decoder) throws {
        address_ip = try? decoder.decode("address_ip")
        name_tv = try? decoder.decode("name_tv")
        pas_tv = try? decoder.decode("pas_tv")
    }
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address_ip != nil{
            dictionary["address_ip"] = address_ip
        }
        if name_tv != nil{
            dictionary["name_tv"] = name_tv
        }
        if pas_tv != nil{
            dictionary["pas_tv"] = pas_tv
        }
        return dictionary
    }
}

struct Token {
    
    fileprivate let userDefaults: UserDefaults
    
    var email: String? {
        get {
            return userDefaults.string(forKey: UserDefaultKey.kEmail.rawValue)
        }
        set {
            userDefaults.set(newValue, forKey: UserDefaultKey.kEmail.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var password: String? {
        get {
            return userDefaults.string(forKey: UserDefaultKey.kPassword.rawValue)
        }
        set {
            userDefaults.set(newValue, forKey: UserDefaultKey.kPassword.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var token: String? {
        get {
            return userDefaults.string(forKey: UserDefaultKey.kAccessToken.rawValue) ?? ""
        }
        set {
            userDefaults.set(newValue, forKey: UserDefaultKey.kAccessToken.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var tokenExists: Bool {
        
        guard let token = self.token, token.count > 0 else {
            return false
        }
        return true
    }
    
    var currentTV: TVModel? {
        get {
            guard let dict = userDefaults.dictionary(forKey: UserDefaultKey.kCurUser.rawValue), let user = dict.toCodableObject() as TVModel? else { return nil}
            return user
        }
        set {
            userDefaults.set(newValue?.toDictionary() ?? [:], forKey: UserDefaultKey.kCurUser.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var current_language: String? {
        get {
            return userDefaults.string(forKey: "UserDefaultKey.kLanguage.rawValue") ?? "vi"
        }
        set {
            userDefaults.set(newValue, forKey: "UserDefaultKey.kLanguage.rawValue")
            userDefaults.synchronize()
        }
    }
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    init() {
        self.userDefaults = UserDefaults.standard
    }
    
    func clear() {
        for key in UserDefaultKey.allCases {
            userDefaults.removeObject(forKey: key.rawValue)
            userDefaults.synchronize()
        }
        
    }
}

//MARK:- ENUM
enum UserDefaultKey: String, CaseIterable {
    case kAccessToken = "ACCESS_TOKEN"
    case kUserID = "kUserID"
    case kCurUser = "USER"
    case kAddress = "ADDRESS"
    case kEmail = "didEmailUser"
    case kPassword = "didPasswordUser"
    case kName = "didNameUser"
    case kFacebookId = "didFacebookIdUser"
    case kGoogleId = "didGoogleIdUser"
    case kAvatar = "didAvatarUser"
    case kCompleteGuide = "kCompleteGuide"
    case kCurrentAppVersion = "kCurrentAppVersion"
    case kUserInToken = "kUserInToken"
    case kStates = "kStates"
    case kPassWord = "kPassWord"
    case kMessenger = "kMessenger"


}
