//
//  YoutubeSearch.swift
//  DemoApp
//
//  Created by Kiu on 7/19/21.
//

import Foundation

struct SearchResponse : Codable {
    
    var etag : String?
    var items : [YTItem]?
    var kind : String?
    var nextPageToken : String?
    var pageInfo : PageInfo?
    var regionCode : String?
    
    init(from decoder: Decoder) throws {
        etag = try? decoder.decode("etag")
        items = try? decoder.decode("items")
        kind = try? decoder.decode("kind")
        nextPageToken = try? decoder.decode("nextPageToken")
        pageInfo = try? decoder.decode("pageInfo")
        regionCode = try? decoder.decode("regionCode")
    }
}

struct PageInfo : Codable {
    
    var resultsPerPage : Int?
    var totalResults : Int?
    
    init(from decoder: Decoder) throws {
        resultsPerPage = try? decoder.decode("resultsPerPage")
        totalResults = try? decoder.decode("totalResults")
    }
}

struct YTItem : Codable {
    
    var etag : String?
    var id : YoutubeId?
    var kind : String?
    var snippet : Snippet?
    
    init(from decoder: Decoder) throws {
        etag = try? decoder.decode("etag")
        id = try? decoder.decode("id")
        kind = try? decoder.decode("kind")
        snippet = try? decoder.decode("snippet")
    }
}

struct YoutubeId : Codable {
    
    var kind : String?
    var videoId : String?
    
    init(from decoder: Decoder) throws {
        kind = try? decoder.decode("kind")
        videoId = try? decoder.decode("videoId")
    }
}


struct Snippet : Codable {
    
    var channelId : String?
    var channelTitle : String?
    var description : String?
    var liveBroadcastContent : String?
    var publishedAt : String?
    var publishTime : String?
    var thumbnails : Thumbnail?
    var title : String?
    
    init(from decoder: Decoder) throws {
        channelId = try? decoder.decode("channelId")
        channelTitle = try? decoder.decode("channelTitle")
        description = try? decoder.decode("description")
        liveBroadcastContent = try? decoder.decode("liveBroadcastContent")
        publishedAt = try? decoder.decode("publishedAt")
        publishTime = try? decoder.decode("publishTime")
        thumbnails = try? decoder.decode("thumbnails")
        title = try? decoder.decode("title")
    }
}

struct Thumbnail : Codable {
    
    var defaultThumbnail : ThumbnailDetail?
    var high : ThumbnailDetail?
    var medium : ThumbnailDetail?
    
    init(from decoder: Decoder) throws {
        defaultThumbnail = try? decoder.decode("default")
        high = try? decoder.decode("high")
        medium = try? decoder.decode("medium")
    }
}

struct ThumbnailDetail : Codable {
    
    var height : Int?
    var url : String?
    var width : Int?
    
    init(from decoder: Decoder) throws {
        height = try? decoder.decode("height")
        url = try? decoder.decode("url")
        width = try? decoder.decode("width")
    }
}
