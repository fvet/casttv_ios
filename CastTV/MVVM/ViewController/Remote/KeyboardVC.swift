//
//  KeyboardVC.swift
//  CastTV
//
//  Created by Admin on 10/08/2021.
//

import UIKit

class KeyboardVC: UIViewController {
    @IBOutlet weak var tfKey: UITextField!
    var viewModel = RemoteViewModel()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Nhập"
        self.tfKey.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)

        self.tfKey.becomeFirstResponder()
//        self.viewModel.getKeyboardText { result in
//            switch result {
//            case .success:
//                self.view.makeToast("Success")
//                break
//            default:
//                self.view.makeToast("Error")
//                break
//            }
//
//        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.sendKey(self.tfKey.text ?? "")
        }
    }
    func sendKey(_ key: String) {
        viewModel.sendTextKeyBoard(key: key) { result in
            switch result {
            case .success:
//                self.view.makeToast("Success")
                break
            default:
//                self.view.makeToast("Error")
                break
            }
            
        }
    }

}
extension KeyboardVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.sendKey(tfKey.text ?? "")
        return true
    }
}
