//
//  CastItemCLCell.swift
//  CastTV
//
//  Created by Kiu on 7/19/21.
//

import UIKit

class CastItemCLCell: UICollectionViewCell {

    @IBOutlet weak var imgPremium: UIImageView!
    @IBOutlet weak var imgCast: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fillData(_ model : CastType) {
        imgCast.image = model.getIcon()
        lbTitle.text = model.rawValue
        imgPremium.isHidden = !model.requiredPremium()
    }
}
